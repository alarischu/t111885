package t111885.y1.pos;

import java.util.Iterator;
import java.util.Map.Entry;
import t111885.y1.wms.Item;
import t111885.y1.wms.ItemSet;

/**
 * Tellimus (Order) on kogum kaupadest ja kogustest.
 * @author Alari Schu
 *
 */
public class Order {
	/** Tellimuse andmestruktuuri objekt kaupade 
	 * ja koguste hoidmiseks. */
	protected ItemSet items;
	
	/** Unikaalne tellimuse järjekorranumber. */
	protected int id;
	
	/** Tellimuste loendur. Suurendatakse ühe võrra 
	 * pärast iga uue tellimuse objekti loomist. */
	private static int counter = 0;

	/**
	 * Konstruktor uue tellimuse (Order) objekti
	 * loomiseks.
	 */
	public Order() {
		id = getNextId();
		items = new ItemSet();
	}
	
	/**
	 * Meetod tellimuse numbri saamiseks.
	 * @return	Tellimuse number (Id).
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Meetod järgmise tellimuse numbri (Id)
	 * saamiseks.
	 * @return	Järgmine tellimuse number (Id).
	 */
	private int getNextId() {
		return ++counter;
	}
	
	/**
	 * Meetod tellimuse andmestruktuuri 
	 * iteraatori saamiseks.
	 * @return	Tellimuse andmestruktuuri iteraator.
	 */
	public Iterator<Entry<Item, Integer>> iterator() {
		return items.getIterator();
	}
	
	/**
	 * Meetod tellimusele ühe ühiku kauba lisamiseks. Kui sama kaup
	 * tellimuses juba eksisteeris, saab kauba uueks koguseks 1. Et 
	 * lisamine õnnestuks, ei tohi kaup olla null.
	 * @param item	Tellimusele lisatav kaup.
	 * @return		True, kui kauba lisamine õnnestus. 
	 * 				False, kui kauba lisamine ei õnnestunud.
	 */
	public boolean add(Item item) {
		return add(item, 1);
	}

	/**
	 * Meetod tellimusele kauba ja koguse lisamiseks. Kui sama kaup
	 * tellimuses juba eksisteeris, muudetakse selle kauba kogust
	 * vastavalt meetodi väljakutses toodud kogusele qty. Et lisamine
	 * õnnestuks, ei tohi kaup olla null ning kauba kogus väiksem 
	 * kui 1.
	 * @param item	Tellimusele lisatav kaup.
	 * @param qty	Tellimusele lisatava kauba kogus.
	 * @return		True, kui kauba/koguse lisamine õnnestus.
	 * 				False, kui kauba/koguse lisamine ei õnnestunud.
	 */
	public boolean add(Item item, int qty) {
		try {
			items.add(item, qty);
		} catch (IllegalArgumentException e) {
			return false;
		}
		return true;
	}

	/**
	 * Meetod tekstilise kirjelduse saamiseks tellimuse
	 * oleku kohta (tellimuse nr, kaubad ja kogused).
	 */
	@Override
	public String toString() {
		return "Order id = " + id + ", items = [" + items + "]";
	}

	/**
	 * Genereeritud automaatselt Eclipse pooolt.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((items == null) ? 0 : items.hashCode());
		return result;
	}

	/**
	 * Võrdleb kahte tellimuse objekti. Tellimused on võrdsed, kui 
	 * objektimuutujad viitavad samadele objektidele mälus või kui mõlemal
	 * objektil on sama tellimuse number, nende andmestruktuurid on 
	 * võrdsed ning võrreldav objekt obj on Order tüüpi objekt.
	 * Genereeritud automaatselt Eclipse poolt.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Order))
			return false;
		Order other = (Order) obj;
		if (id != other.id)
			return false;
		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items))
			return false;
		return true;
	}
	

}
