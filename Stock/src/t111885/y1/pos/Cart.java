package t111885.y1.pos;

import java.util.Iterator;
import java.util.Map.Entry;

import t111885.y1.wms.Item;

/**
 * Ostukorvi (Cart) saab kasutada veebipoes kliendi ostusoovide 
 * salvestamiseks.
 * 
 * @author Alari Schu
 *
 */
public class Cart extends Order {
	
	/**
	 * Meetod korvi lisatud toote koguse muutmiseks. Muutmise 
	 * õnnestumiseks peab kaup eksisteerima ostukorvis ning kauba
	 * uus kogus peab olema suurem kui 0.
	 * @param item	Kaup, mille kogust soovitakse muuta.
	 * @param qty	Uus kogus kaubale ostukorvis.
	 * @return		True, kui muutmine õnnestus.
	 * 				False, kui muutmine ei õnnestunud.
	 */
	public boolean change(Item item, int qty) {
		
		if (items.containsKey(item)) {
			return add(item, qty);
		} else {
			return false;
		}
		
	}
	
	/**
	 * Meetod kauba eemaldamiseks ostukorvist. Et eemaldamine
	 * õnnestuks, peab kaup eksisteerima ostukorvis.
	 * @param item	Kaup, mida soovitakse eemaldada.
	 * @return		True, kui kauba eemaldamine õnnestus.
	 * 				False, kui kauba eemaldamine ei õnnestunud.
	 */
	public boolean remove(Item item) {

		if (items.containsKey(item)) {
			items.remove(item);
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Meetod ostukorvi täielikuks tühjendamiseks.
	 */
	public void clear() {
		
		items.clear();
	}
	
	/**
	 * Meetod ostukorvi maksumuse arvutamiseks.
	 * @return	Ostkukorvi maksumus.
	 */
	public double getTotal() {
		
		double total = 0.0;
		Iterator<Entry<Item, Integer>> iter = items.getIterator();
		while (iter.hasNext()) {
			Entry<Item, Integer> entry = iter.next();
			double price = entry.getKey().getPrice();
			int qty = entry.getValue();
			total += qty * price;
		}
		return total;
	}
	
	/**
	 * Meetod ostukorvi sisu tellimuseks vormistamiseks.
	 * @return	Ostukorvi sisu tellimusena.
	 */
	public Order checkOut() {
		
		Order order = new Order();
		Iterator<Entry<Item, Integer>> iter = iterator();
		Entry<Item, Integer> entry;
		
		while (iter.hasNext()) {
			entry = iter.next();
			order.add(entry.getKey(), entry.getValue());
		}
		
		return order;
	}
	
	/**
	 * Meetod kontrollib, kas ostukorv on tühi.
	 * @return	True, kui korv on tühi.
	 * 			False, kui korv ei ole tühi.
	 */
	public boolean isEmpty() {
		return items.isEmpty();
	}

	/**
	 * Meetod tekstilise kirjelduse saamiseks ostukorvi
	 * oleku kohta (ostukorvi nr, kaubad ja kogused).
	 */
	@Override
	public String toString() {
		return "Cart id = " + id + ", items = [" + items + "]";
	}
	
	/**
	 * Võrdleb kahte ostukorvi objekti. Ostukorvid on võrdsed, kui 
	 * objektimuutujad viitavad samadele objektidele mälus või kui 
	 * mõlemal objektil on sama ostukorvi number, nende andme-
	 * struktuurid on võrdsed ning võrreldav objekt obj on Cart 
	 * tüüpi objekt.
	 * Genereeritud automaatselt Eclipse poolt.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Cart))
			return false;
		Cart other = (Cart) obj;
		if (id != other.id)
			return false;
		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items))
			return false;
		return true;
	}
}
