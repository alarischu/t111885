package t111885.y1.wms;

/**
 * Kaup (Item) on ostetav ja müüdav ese, mida kirjeldab nimetus ja 
 * hind.
 * @author Alari Schu
 *
 */
public class Item {
	/** Kauba nimetus. */
	private String title;
	/** Kauba hind. */
	private double price;
	
	/**
	 * Konstruktor kauba kiireks ja mugavaks loomiseks.
	 * @param title	 	Kauba nimetus.
	 * @param price		Kauba hind.
	 * @throws IllegalArgumentException 
	 * Kui kauba nimetus on tühi või null. Kui kauba 
	 * hind on negatiivne.
	 */
	public Item(String title, double price) 
			throws IllegalArgumentException {
		
		if (title == null || title.isEmpty()) {
			throw new IllegalArgumentException("Kauba nimetus " +
					"ei tohi olla tühi ega null");
		}
		
		if (price < 0.0) {
			throw new IllegalArgumentException("Kauba hind ei " +
					"tohi olla negatiivne");
		}
		
		this.title = title;
		this.price = price;
	}
	
	/**
	 * Meetod kauba nimetuse saamiseks.
	 * @return	Kauba nimetus.
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Meetod kauba hinna saamiseks.
	 * @return	Kauba hind.
	 */
	public double getPrice() {
		return price;
	}
	
	/*
	 * Meetod kahe kauba võrdlemiseks. Kaubad on võrdsed, kui nende
	 * nimetused ja hinnad on võrdsed ning kui võrreldav objekt obj
	 * on Item tüüpi objekt. 
	 * Genereeritud automaatselt Eclipse poolt.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Item))
			return false;
		Item other = (Item) obj;
		if (Double.doubleToLongBits(price) != Double
				.doubleToLongBits(other.price))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
	
	/**
	 * Meetod tekstilise kirjelduse saamiseks kauba
	 * kohta (kauba nimetus ja hind).
	 */
	@Override
	public String toString() {
		return title + " " + price;
	}
	
	/*
	 * Genereeritud automaatselt Eclipse poolt.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(price);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

}
