package t111885.y1.wms;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import t111885.y1.pos.Order;

/**
 * Ladu (Stock) talletab endas kõikide ladustatud kaupade laoseisu.
 * @author Alari Schu
 *
 */
public class Stock {
	
	/** Ladustatave kaupade andmestruktuur. */
	private ItemSet items = new ItemSet();
	
	/** Kaupade laoliikumiste andmestruktuur. */
	private ArrayList<Transaction> trans = new ArrayList<Transaction>();
	
	/**
	 * Meetod kaupade lattu arvele võtmiseks. Kaupade laoseis
	 * suureneb vastavalt tellimusel märgitud kogustele. Arvele võtmise 
	 * õnnestumiseks ei tohi tellimus olla null.
	 * @param order	Tellimus, mille alusel toimub kauba vastu võtmine.
	 * @return		True, kui lattu arvele võtmine õnnestus.
	 * 				False, kui lattu arvele võtmine ei õnnestunud.
	 */
	public boolean receive(Order order) {
		return update(true, order);
	}

	/**
	 * Meetod kaupade laost väljastamiseks. Kaupade laoseis
	 * väheneb vastavalt tellimusel märgitud kogustele. Laost 
	 * väljastamise õnnestumiseks ei tohi tellimus olla null,
	 * kaup peab laos eksisteerima ning kaupa peab laos olema
	 * tellimuses märgitud koguse jagu.
	 * @param order	Tellimus, mille alusel toimub kauba väljastamine.
	 * @return		True, kui laost väljastamine õnnestus.
	 * 				False, kui laost väljastamine ei õnnestunud.
	 */
	public boolean dispatch(Order order) {
		return update(false, order);
	}
	
	/**
	 * Meetod kauba laoseisu uuendamiseks nii kaupade arvele 
	 * võtmisel kui väljastamisel. Et laoseisu uuendamine 
	 * õnnestuks, ei tohi tellimus olla null, kauba väljastamisel
	 * peab kaup laos eksisteerima ning kaupa peab laos olema
	 * tellimuses märgitud koguse jagu.
	 * @param receive	Kui toimub kauba arvele võtmine, siis true.
	 * 					Kui toimub kauba väljastamine, siis false.
	 * @param order		Tellimus, mis tingib kaupade laoseisu 
	 * 					uuendamise.
	 * @return			True, kui laoseisu uuendamine õnnestus.
	 * 					False, kui laoseisu uuendamine ei õnnestunud.
	 */
	private boolean update(boolean receive, Order order) {
		boolean updateNoErrors = true; 
		
		if (order == null) {
			return false;
		}
		Iterator<Entry<Item, Integer>> iter = order.iterator();
		while (iter.hasNext()) {
			Entry<Item, Integer> entry = iter.next();
			Item item = entry.getKey();
			int qty = entry.getValue();

			if (items.containsKey(item)) {
				
				/* Kui selline kaup on juba 
				laos olemas, leia uus kauba kogus. */
				int currentQty = items.get(item);
				int newQty = (receive) ? 
						(currentQty + qty) : (currentQty - qty);
					
				if (newQty == 0) {
					
					/* Kui kauba uus laoseis on 0, 
					eemalda kaup laost. */
					items.remove(item);
				} else if (newQty < 0) {
					
					/* Kui kauba uus laoseis on
					negat. eemalda kaup laost
					ja anna ebaõnnestumisest
					märku. */
					newQty = 0;
					items.remove(item);
					updateNoErrors = false;
				} else {
					
					// Muuda kauba laoseisu.
					items.put(item, newQty);
				}
				trans.add(new Transaction(item, newQty - currentQty, order));
			} else {
				
				/* Selline kaup laos
				seni puudus. */
				if (receive) {
					
					// Võta kaup vastu.
					items.put(item, qty);
					trans.add(new Transaction(item, qty, order));
				} else {
					
					// Kaupa ei saa väljastada.
					updateNoErrors = false;
				}
				
			}
		}
		return updateNoErrors;
	}
	
	/**
	 * Meetod ettantud kauba laoseisu saamiseks 
	 * antud ajahetkel.
	 * @param item	Kaup, mille laoseisu tahetakse saada.
	 * @return		Kauba laoseisu kogus.
	 */
	public int getAvailable(Item item) {
		if (items.containsKey(item)) {
			return items.get(item);
		} else {
			return 0;
		}
	}
	
	/**
	 * Meetod erinevate kaubaartiklite arvu leidmiseks.
	 * @return	Erinevate kaubaartiklite arv. Näiteks kui
	 * laos on 2 piima, 5 leiba ja 1 kala siis tagastatakse
	 * kaubaartiklite arvuks 3. 
	 */
	public int getTotalArticleQty() {
		return items.size();
	}
	
	/**
	 * Meetod kauba laoliikumiste leidmiseks. 
	 * @param item	Kaup, mille laoliikumist leitakse.
	 * @return		Kõik etteantud kaubaga seotud 
	 * 				laoliikumised.
	 */
	public List<Transaction> getItemTransactions(Item item) {
		List<Transaction> itemTransactions = 
				new ArrayList<Transaction>();
		
		for (Transaction t : trans) {
			if (t.getItem().equals(item)) {
				itemTransactions.add(t);
			}
		}
		return itemTransactions;
	}
	
	/**
	 * Meetod kauba laoliikumiste arvu leidmiseks.
	 * @param item	Kaup, mille laoliikumiste arvu
	 * 				leitakse.
	 * @return		Etteantud kaubaga seotud 
	 * 				laoliikumiste arv.
	 */
	public int getItemTransactionsCount(Item item) {
		return getItemTransactions(item).size();
	}
	
	/**
	 * Meetod, mis kontrollib kas ladu on tühi.
	 * @return	True, kui ladu on tühi.
	 * 			False, kui ladu ei ole tühi.
	 */
	public boolean isEmpty() {
		return items.size() == 0;
	}
	
	/**
	 * Meetod tekstilise kirjelduse saamiseks
	 * lao oleku kohta (kaubad ja kogused).
	 * @return	Tekstiline kirjeldus lao kohta.
	 */
	@Override
	public String toString() {
		return "Stock items = [" + items.toString() +
		"]";
	}

}
