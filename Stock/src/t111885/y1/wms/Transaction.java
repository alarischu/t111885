package t111885.y1.wms;

import t111885.y1.pos.Order;

/**
 * Kauba laoliikumist (Transaction) iseloomustab toode, mida liigutati, 
 * kogus ning viide tellimusele, mis liikumise põhjustas. 
 * @author Alari Schu
 *
 */
public class Transaction {
	private Item item;
	private int qty;
	private Order order;
	
	/**
	 * Konstruktor laoliikumise objekti kiireks ja mugavaks loomiseks.
	 * @param item		Laoliikumises olev kaup.
	 * @param qty		Kauba kogus.
	 * @param order		Tellimus, mis põhjustas laoliikumise.
	 * @throws IllegalArgumentException Kui kaup või tellimus on null.
	 */
	public Transaction(Item item, int qty, Order order) 
			throws IllegalArgumentException {
		if (item == null || order == null) {
			throw new IllegalArgumentException("Kaup või tellimus " +
					"ei tohi olla null");
		}
		this.item = item;
		this.qty = qty;
		this.order = order;
	}
	
	/**
	 * Meetod laoliikumises olnud kauba saamiseks.
	 * @return	Kaup, mis oli laoliikumises.
	 */
	public Item getItem() {
		return item;
	}
	
	/**
	 * Meetod laoliikumises olnud kauba koguse saamiseks.
	 * @return	Kauba kogus, mis oli laoliikumises.
	 */
	public int getQuantity() {
		return qty;
	}
	
	/**
	 * Meetod laoliikumise põhjustanud tellimuse
	 * saamiseks.
	 * @return	Tellimus, mis põhjustas laoliikumise.
	 */
	public Order getOrder() {
		return order;
	}

	/*
	 * Genereeritud automaatselt Eclipse poolt.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((item == null) ? 0 : item.hashCode());
		result = prime * result + ((order == null) ? 0 : order.hashCode());
		result = prime * result + qty;
		return result;
	}

	/* 
	 * Meetod kahe laoliikumise võrdlemiseks. Laoliikumised on võrdsed, 
	 * kui võrreldavad objektimuutujad viitavad samale objektile või
	 * kui laoliikumisi iseloomustavad kaup, kogus ja tellimus on võrdsed ning 
	 * kui võrreldav objekt obj on Transaction tüüpi objekt. 
	 * Genereeritud automaatselt Eclipse poolt. */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Transaction))
			return false;
		Transaction other = (Transaction) obj;
		if (item == null) {
			if (other.item != null)
				return false;
		} else if (!item.equals(other.item))
			return false;
		if (order == null) {
			if (other.order != null)
				return false;
		} else if (!order.equals(other.order))
			return false;
		if (qty != other.qty)
			return false;
		return true;
	}
}
