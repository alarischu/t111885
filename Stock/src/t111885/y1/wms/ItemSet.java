package t111885.y1.wms;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Tellimuse (Order) andmestruktuur kaupade ja koguste hoidmiseks.
 * @author Alari Schu
 */
public class ItemSet extends HashMap<Item, Integer>  {

	private static final long serialVersionUID = -109768164377430176L;

	/**
	 * Meetod kauba lisamiseks andmestruktuuri. Lisab ühe
	 * ühiku kaupa.
	 * @param item	Andmestruktuuri lisatav kaup.
	 */
	public void add(Item item) {
		add(item, 1);
	}
	
	/**
	 * Meetod kaupade ja koguste lisamiseks andmestruktuuri.
	 * @param item	Andmestruktuuri lisatav kaup.
	 * @param qty	Andmestruktuuri lisatava kauba kogus.
	 * @throws IllegalArgumentException	Kui kaup on null või 
	 * 				kui kauba kogus on väiksem kui 1.
	 */
	public void add(Item item, int qty) 
			throws IllegalArgumentException {
		if (item == null) {
			throw new IllegalArgumentException("Kaup ei tohi " +
					"olla null");
		} else if (qty < 1) {
			throw new IllegalArgumentException("Kauba kogus peab olema " +
					"suurem nullist");
		} else {
			put(item, qty);
		}
	}
	
	/**
	 * Meetod andmestruktuuri iteraatori saamiseks.
	 * @return	Andmestruktuuri iteraator.
	 */
	public Iterator<Entry<Item, Integer>> getIterator() {
		Set<Entry<Item, Integer>> set = entrySet();
		return set.iterator();
	}
	
	/**
	 * Meetod tekstilise kirjelduse saamiseks andmestruktuuri
	 * oleku kohta (kaubad ja kogused).
	 */
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		Iterator<Entry<Item, Integer>> iter = getIterator();
		while (iter.hasNext()) {
			Entry<Item, Integer> entry = iter.next();
			Item item = entry.getKey();
			int qty = entry.getValue();
			buf.append(qty).append("x (").append(item).append(")");
			if (iter.hasNext()) {
				buf.append(", ");
			}
		}
		return buf.toString();
	}
	
}
