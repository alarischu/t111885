package t111885.y1.test;

import t111885.y1.pos.Order;
import t111885.y1.wms.Item;

/**
 * Klass kaupadest väikese sortimendi ja tellimuste loomiseks. Klassi
 * staatilised liikmed ja meetodid on abiks JUnit testklassidele
 * tellimuste ja kaupade mugavamaks loomiseks.
 * @author Alari Schu
 *
 */
public class Orders {
	
	public static final Item PIIM = new Item("Piim", 0.55);
	public static final Item LEIB = new Item("Leib", 0.95);
	public static final Item JUUST = new Item("Juust", 7.49);
	public static final Item KALA = new Item("Kala", 5.85);
	public static final Item MARGARIIN = new Item("Margariin", 1.25);

	/**
	 * Meetod kaupade sortimendist väikse tellimuse
	 * loomiseks.
	 * @return	Tellimus 5 piimast ja 1 leivast.
	 */
	public static Order getOrder1() {
		Order order = new Order();
		order.add(PIIM, 5);
		order.add(LEIB);
		return order;
	}
	
	/**
	 * Meetod kaupade sortimendist väikse tellimuse
	 * loomiseks.
	 * @return	Tellimus 1 juustust ja 2 leivast.
	 */
	public static Order getOrder2() {
		Order order = new Order();
		order.add(JUUST);
		order.add(LEIB, 2);
		return order;
	}
	
	/**
	 * Meetod kaupade sortimendist väikse tellimuse
	 * loomiseks.
	 * @return	Tellimus 3 piimast, 1 leivast ja 1
	 * 			juustust.
	 */
	public static Order getOrder3() {
		Order order = new Order();
		order.add(PIIM, 3);
		order.add(LEIB);
		order.add(JUUST);
		return order;
	}
	
	/**
	 * Meetod kaupade sortimendist väikse tellimuse
	 * loomiseks.
	 * @return	Tellimus 2 leivast.
	 */
	public static Order getOrder4() {
		Order order = new Order();
		order.add(LEIB, 2);
		return order;
	}
	
	/**
	 * Meetod kaupade sortimendist väikse tellimuse
	 * loomiseks.
	 * @return	Tellimus 1 margariinist ja 1 kalast.
	 */
	public static Order getOrder5() {
		Order order = new Order();
		order.add(MARGARIIN);
		order.add(KALA);
		return order;
	}

}
