package t111885.y1.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import t111885.y1.wms.Item;
import t111885.y1.wms.ItemSet;

/**
 * Kaupade (Item) andmestruktuuri klassi testimiseks.
 * @author Alari Schu
 *
 */
public class ItemSetTest {
	ItemSet items;
	
	/* Meetod testib kaupade lisamist. */
	@Test
	public void testAdd() {
		items = new ItemSet();
		Item i1 = new Item("Leib", 0.95);
		Item i2 = new Item("Piim", 0.55);
		Item i3 = new Item("Sai", 0.65);
		
		items.add(i1);
		items.add(i2, 5);
		assertTrue(items.containsKey(i1) && items.get(i1) == 1);
		assertTrue(items.containsKey(i2) && items.get(i2) == 5);
		assertEquals(items.size(), 2);
		
		items.add(i1, 3);	// Lisame olemasoleva toote.
		assertEquals(items.size(), 2);
		assertTrue(items.get(i1) == 3);
		
		items.add(i3);	// Lisame uue toote.
		assertEquals(items.size(), 3);
	}

	/* Meetod testib andmestruktuuri oleku (kaupade ja koguste)
	 * tekstilise kirjelduse saamist. */
	@Test
	public void testToString() {
		testAdd();
		System.out.println(items);
		assertEquals(items.toString(), "5x (Piim 0.55), 1x (Sai 0.65), 3x (Leib 0.95)");
	}
	
	/* Meetod võrdleb andmestruktuure. */
	@Test
	public void testEqualsAndHashCode() {
		ItemSet s1 = new ItemSet();
		s1.add(new Item("Leib", 0.95));
		s1.add(new Item("Piim", 0.55), 3);

		ItemSet s2 = new ItemSet();
		s2.add(new Item("Piim", 0.55), 3);
		s2.add(new Item("Leib", 0.95));
		
		assertEquals(s1, s2);
		assertTrue(s1.hashCode() == s2.hashCode());
		
		s2.add(new Item("Sai", 0.65));
		assertTrue(!s1.equals(s2));
		assertTrue(s1.hashCode() != s2.hashCode());
	}

}
