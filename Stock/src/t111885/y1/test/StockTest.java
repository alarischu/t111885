package t111885.y1.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import t111885.y1.pos.Order;
import t111885.y1.wms.Item;
import t111885.y1.wms.Stock;
import t111885.y1.wms.Transaction;

/**
 * Lao klassi (Stock) testimiseks.
 * @author Alari Schu
 *
 */
public class StockTest {

	/**
	 * Meetod kauba laoliikumiste kontrolliks.
	 * @param trans	Kaubaliikumiste kogum.
	 * @param index	Kaubaliikumise indeks, mida kontrollitakse.
	 * @param item	Kaup, mille liikumist kontrollitakse.
	 * @param qty	Kontrollitava kauba kogus.
	 * @param order	Tellimus, mis tingis kauba liikumise laos.
	 */
	public static void checkItemTransactions(List<Transaction> trans, 
			int index, Item item, int qty, Order order) {
		
		assertEquals(trans.get(index).getItem(), item);
		assertTrue(trans.get(index).getQuantity() == qty);
		assertEquals(trans.get(index).getOrder(), order);
	}
	
	/* Meetod testib kauba vastuvõtmist. */
	@Test
	public void testReceive() {
		
		Stock s = new Stock();
		assertTrue(s.isEmpty());
		assertTrue(!s.receive(null));
		s.receive(Orders.getOrder1());	// Lattu võeti 5 piima ja 1 leib.
		assertEquals(s.getAvailable(Orders.PIIM), 5);
		assertEquals(s.getAvailable(Orders.LEIB), 1);
		assertEquals(s.getTotalArticleQty(), 2);
		
		s.receive(Orders.getOrder2());	// Lattu võeti 1 juust ja 2 leiba.
		
		/* Laos peab nüüd olema 5 piima, 3 leiba ja 1 juust. */
		assertEquals(s.getAvailable(Orders.PIIM), 5);
		assertEquals(s.getAvailable(Orders.LEIB), 3);
		assertEquals(s.getAvailable(Orders.JUUST), 1);
		assertEquals(s.getTotalArticleQty(), 3);	// Kokku 3 erin. toodet.
	}

	/* Meetod testib kauba väljastamist */
	@Test
	public void testDispatch() {
		Stock s = new Stock();
		assertTrue(s.isEmpty());
		
		/* Laost puuduva kauba väljastamise ja 
		 * tagastusväärtuse kontroll. */
		assertTrue(!s.dispatch(Orders.getOrder5()));
		assertTrue(s.getTotalArticleQty() == 0);
		
		assertTrue(s.receive(Orders.getOrder1()));
		assertTrue(s.receive(Orders.getOrder2()));	
		assertEquals(s.getTotalArticleQty(), 3);
				// Laos on kokku nüüd 5 piima, 3 leiba ja 1 juust.
		
		/* Väljastame 3 piima, 1 leiva ja 1 juustu. */
		assertTrue(s.dispatch(Orders.getOrder3()));	
		
		/* Kontrollime laoseisu. */
		assertEquals(s.getAvailable(Orders.PIIM), 2);
		assertEquals(s.getAvailable(Orders.LEIB), 2);
		assertEquals(s.getAvailable(Orders.JUUST), 0);
		assertEquals(s.getTotalArticleQty(), 2);
		
		/* Proovime väljastada 5 piima, mis tingiks
		 * piima laoseisu muutumist negatiivseks.
		 * Kontrollime tagastusväärtust ja piima
		 * laoseisu, mis peab olema 0. */
		Order o = new Order();
		o.add(Orders.PIIM, 5);
		assertTrue(!s.dispatch(o));
		assertEquals(s.getAvailable(Orders.PIIM), 0);
		assertEquals(s.getTotalArticleQty(), 1);
		assertEquals(s.getAvailable(Orders.LEIB), 2);
		
		/* Väljastame viimased 2 leiba.
		 * Kontrollime, et ladu oleks tühi ning 
		 * tagastustüüp ei annaks teada vigadest
		 * kauba väljastamisel. */
		assertTrue(s.dispatch(Orders.getOrder4()));
		assertTrue(s.isEmpty());
	}
	
	/* Meetod testib laoliikumisi. */
	@Test
	public void testItemTransactions() {
		Stock s = new Stock();
		Order o1 = Orders.getOrder1();
		Order o2 = Orders.getOrder3();
		s.receive(o1);	// + 5 piima ja 1 leib
		s.dispatch(o2);	// - 3 piima, 1 leib ja 1 juust
		s.dispatch(o1);	// - 5 piima ja 1 leib
			// Laoseis peaks nüüd olema 0
		assertEquals(s.getTotalArticleQty(), 0);
		
		/* Kontrollime piima liikumisi laos */
		List<Transaction> t = 
				s.getItemTransactions(Orders.PIIM);
		assertTrue(s.getItemTransactionsCount(Orders.PIIM) == 3);

		/* 1. liikumine sisse (+5) */
		checkItemTransactions(t, 0, Orders.PIIM, 5, o1);

		/* 2. liikumine välja (-3) */
		checkItemTransactions(t, 1, Orders.PIIM, -3, o2);

		/* 3. liikumine välja (-2) */
		checkItemTransactions(t, 2, Orders.PIIM, -2, o1);
		
		/* Kontrollime leiva liikumisi laos */
		t = s.getItemTransactions(Orders.LEIB);
		assertTrue(s.getItemTransactionsCount(Orders.LEIB) == 2);
		
		/* 1. liikumine sisse (+1) */
		checkItemTransactions(t, 0, Orders.LEIB, 1, o1);
		
		/* 2. liikumine välja (-1) */
		checkItemTransactions(t, 1, Orders.LEIB, -1, o2);

		/* Kontrollime juustu liikumisi laos */
		t = s.getItemTransactions(Orders.JUUST);
		assertTrue(s.getItemTransactionsCount(Orders.JUUST) == 0);
		
	}

}
