package t111885.y1.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import t111885.y1.wms.Item;

/**
 * Kauba klassi (Item) testimiseks.
 * @author Alari Schu
 *
 */
public class ItemTest {

	@Test
	public void testGetters() {
		Item item = new Item("Leib", 0.95);
		assertEquals("Leib", item.getTitle());
		assertTrue(item.getPrice() == 0.95);
	}

	@Test
	public void testEqualsObject() {
		Item i1 = new Item("Leib", 0.95);
		assertEquals(i1, i1);
		assertEquals(i1, new Item("Leib", 0.95));
		assertEquals(new Item("Leib", 0.95), i1);
		assertTrue(!i1.equals(new Item("Leib", 0.9500001)));
		assertTrue(!i1.equals(new Item("Sai", 0.95)));
		assertTrue(!i1.equals(new Object()));
		assertTrue(!i1.equals(null));
	}

	@Test
	public void testHashCode() {
		Item i1 = new Item("Leib", 0.95);
		assertTrue(i1.hashCode() == i1.hashCode());
		assertTrue(i1.hashCode() == new Item("Leib", 0.95).hashCode());
		assertTrue(i1.hashCode() != new Item("Leib", 0.9500001).hashCode());
		assertTrue(i1.hashCode() != new Item("Sai", 0.95).hashCode());
		assertTrue(i1.hashCode() != new Object().hashCode());
	}
	
	@Test
	public void testToString() {
		Item i1 = new Item("Leib", 0.95);
		assertEquals("Leib 0.95", i1.toString());
	}

}
