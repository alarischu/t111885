package t111885.y1.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Map;

import org.junit.Test;

import t111885.y1.pos.Cart;
import t111885.y1.pos.Order;
import t111885.y1.wms.Item;

/**
 * Ostukorvi (Cart) testimiseks.
 * @author Alari Schu
 *
 */
public class CartTest {

	/* 
	 * Meetod testib kaupade lisamist ostukorvi.
	 */
	@Test
	public void testAdd() {
		Cart c = new Cart();
		c.add(Orders.MARGARIIN);
		c.add(Orders.KALA, 2);
		ArrayList<Map.Entry<Item,Integer>> entries = OrderTest.getEntries(c);
		assertEquals(entries.size(), 2);
		OrderTest.checkOrderEntries(entries, Orders.MARGARIIN, 1);
		OrderTest.checkOrderEntries(entries, Orders.KALA, 2);

	}
	
	/* 
	 * Meetod testib ostukorvis kaupade koguste 
	 * muutmist. */
	@Test
	public void testChange() {
		Cart c = new Cart();
		c.add(Orders.PIIM);
		ArrayList<Map.Entry<Item,Integer>> entries = 
				OrderTest.getEntries(c);
		assertEquals(entries.size(), 1);
		assertTrue(!c.change(Orders.JUUST, 2));	// Toodet ei eksisteeri.
		assertTrue(!c.change(Orders.PIIM, 0));	// Kogus peab olema > 0
		OrderTest.checkOrderEntries(entries, Orders.PIIM, 1);
		assertTrue(c.change(Orders.PIIM, 4));
		OrderTest.checkOrderEntries(entries, Orders.PIIM, 4);

	}

	
	/* 
	 * Meetod testib kaupade eemaldamist ostukorvist.
	 */
	@Test
	public void testRemove() {
		Cart c = new Cart();
		c.add(Orders.PIIM, 3);
		c.add(Orders.LEIB);
		ArrayList<Map.Entry<Item,Integer>> entries = OrderTest.getEntries(c);
		assertEquals(entries.size(), 2);
		assertTrue(!c.remove(Orders.KALA));	// Toodet ei eksisteeri
		assertTrue(c.remove(Orders.PIIM));
		entries = OrderTest.getEntries(c);
		assertTrue(OrderTest.getItemIndex(Orders.PIIM, entries) == -1);
		assertTrue(entries.size() == 1);
		OrderTest.checkOrderEntries(entries, Orders.LEIB, 1);
		assertTrue(c.remove(Orders.LEIB));
		entries = OrderTest.getEntries(c);
		assertTrue(OrderTest.getItemIndex(Orders.LEIB, entries) == -1);
		assertTrue(c.isEmpty());
		
	}

	
	/* 
	 * Meetod testib ostukorvi täielikku tühjendamist. 
	 */
	@Test
	public void testClear() {
		Cart c = new Cart();
		c.add(Orders.KALA, 3);
		c.add(Orders.JUUST);
		c.add(Orders.LEIB, 5);
		assertTrue(!c.isEmpty());
		c.clear();
		assertTrue(c.isEmpty());
	}

	/*
	 * Meetod testib ostukorvi 
	 * maksumuse arvutamist.
	 */
	@Test
	public void testGetTotal() {
		final double DELTA = 0.000001;
		Cart c = new Cart();
		assertEquals(c.getTotal(), 0, DELTA);
		c.add(Orders.PIIM, 3);
		c.add(Orders.LEIB);
		assertEquals(c.getTotal(), 2.60, DELTA);
		c.add(Orders.MARGARIIN, 2);
		assertEquals(c.getTotal(), 5.10, DELTA);
		
	}

	/* Meetod testib ostukorvi sisu 
	 * tellimuseks vormistamist. */
	@Test
	public void testCheckOut() {
		Cart c = new Cart();
		c.add(Orders.LEIB, 2);
		c.add(Orders.MARGARIIN);
		Order o = c.checkOut();
		assertTrue(o instanceof Order);
		ArrayList<Map.Entry<Item,Integer>> entries = 
				OrderTest.getEntries(o);
		assertTrue(entries.size() == 2);
		OrderTest.checkOrderEntries(entries, Orders.MARGARIIN, 1);
		OrderTest.checkOrderEntries(entries, Orders.LEIB, 2);

	}
	
	/* Meetod testib ostukorvide
	 * võrdlust. */
	@Test
	public void testEquals() {
		Cart c1 = new Cart();
		c1.add(Orders.JUUST);
		c1.add(Orders.PIIM);
		
		Cart c2 = new Cart();
		c2.add(Orders.JUUST);
		c2.add(Orders.PIIM);

		Order o1 = (Order) c1;
		Order o2 = c2.checkOut();
		
		assertTrue(!c1.equals(c2));	// Erinev Id
		assertEquals(c1, o1);		// Viitavad samale objektile
		assertEquals(c1, c1);
		assertTrue(!c2.equals(o2));
		
	}
	
}
