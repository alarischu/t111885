package t111885.y1.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;

import t111885.y1.pos.Order;
import t111885.y1.wms.Item;

/**
 * Tellimuse (Order) klassi testimiseks.
 * @author Alari Schu
 *
 */
public class OrderTest {
	
	/**
	 * Meetod tellimusest andmestruktuuri kõikide kannete
	 * kättesaamiseks. 
	 * @param o	Tellimus.
	 * @return	Tellimuse andmestruktuuri sissekanded.
	 */
	public static ArrayList<Map.Entry<Item,Integer>> getEntries(Order order) {
		Iterator<Entry<Item, Integer>> iter = order.iterator();
		ArrayList<Entry<Item, Integer>> entries = new 
				ArrayList<Entry<Item, Integer>>();
		while (iter.hasNext()) {
			Entry<Item, Integer> entry = iter.next();
			entries.add(entry);
		}
		return entries;
	}
	
	/**
	 * Meetod tellimuse andmestruktuuri kannetest kauba indeksi
	 * kättesaamiseks.  
	 * @param item		Kaup, mille indeksit otsitakse.
	 * @param entries	Kaupade/koguste kanded, mille hulgas kaupa
	 * 					otsitakse.
	 * @return			Kauba indeks ehk mitmendas kandes otsitav
	 * 					kaup leidus. Tagastatakse 0, kui otsitavat
	 * 					kaupa ei leitud.
	 */
	public static int getItemIndex(Item item, 
			ArrayList<Entry<Item, Integer>> entries) {
		
		for (int i = 0; i < entries.size(); i++) {
			if (entries.get(i).getKey().equals(item)) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * Meetod kontrollib, kas tellimuse kandes on õige kaup ning kogus.
	 * @param entries	Tellimuse kanded.
	 * @param item		Kaup, mida kannetes kontrollitakse.
	 * @param value		Vastava kauba kogus kandes.
	 */
	public static void checkOrderEntries(ArrayList<Entry<Item, Integer>> entries, 
			Item item, int value) {
		
		int entryIndex = getItemIndex(item, entries);
		assertEquals(entries.get(entryIndex).getKey(), item);
		assertTrue(entries.get(entryIndex).getValue() == value);
	}
	
	@Test
	public void testGetId() {
		Order first = Orders.getOrder1();
		Order second = Orders.getOrder1();
		Order third = Orders.getOrder2();
		assertTrue(first.getId() == 1);
		assertTrue(second.getId() == 2);
		assertTrue(third.getId() == 3);
	}

	@Test
	public void testToString() {
		Order order = Orders.getOrder1();
		assertEquals(order.toString(),
				"Order id = 4, items = [5x (Piim 0.55), 1x (Leib 0.95)]");
	}

	/*
	 * Test samaväärsete tellimuste
	 * kontrollimiseks.
	 */
	@Test
	public void testEqualsObject() {
		Order first = Orders.getOrder1();
		Order second = Orders.getOrder1();
		Order third = Orders.getOrder2();
		assertTrue(!first.equals(third));
		assertTrue(!first.equals(second));	// Mõlemal erinev Id
		assertEquals(first, first);
	}
	

	/* Meetod testib negatiivsete ja 
	 * nulliste kaubakoguste sisestamist. */
	@Test 
	public void testOrder() {
		Order order = new Order();
		assertTrue(!order.add(Orders.KALA,   0));
		assertTrue(!order.add(Orders.MARGARIIN, -3));
	}
	
	/*
	 * Meetodiga lisatakse tellimusele 3
	 * erinevat sorti kaupa ning kontrollitakse
	 * sisestatud kaupa ja koguseid.
	 */
	@Test
	public void testAdd() {
		Order o = new Order();
		o.add(Orders.PIIM, 5);
		o.add(Orders.KALA, 2);
		o.add(Orders.JUUST);
		ArrayList<Map.Entry<Item,Integer>> entries = getEntries(o);
		assertEquals(entries.size(), 3);	// Kokku 3 kannet
		
		checkOrderEntries(entries, Orders.PIIM, 5);
		checkOrderEntries(entries, Orders.KALA, 2);
		checkOrderEntries(entries, Orders.JUUST, 1);

	}
	
}
