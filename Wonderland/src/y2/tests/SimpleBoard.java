package y2.tests;

import y2.game.Board;
import y2.game.ExitWonderLandTile;
import y2.game.Tile;

/**
 * Test abiklass mänguväljakute kiireks loomiseks.
 * @author alari
 *
 */
public class SimpleBoard {

	public static final Tile T1 = new Tile("Narrow Path");
	public static final Tile T2 = new Tile("Old Three");
	public static final Tile T3 = new Tile("Rabbit Hole");
	public static final Tile T4 = new Tile("Bottom of Rabbit Hole");
	public static final Tile T5 = new ExitWonderLandTile("Gate to Paradise");
	
	public static Board createNewBoard() {
		Board b = new Board();
		b.append(T1);
		b.append(T2);
		b.append(T3);
		b.append(T4);
		b.clear();
		return b;
	}
	
	public static Board createNewBoardWithExit() {
		Board b = new Board();
		b.append(T1);
		b.append(T2);
		b.append(T3);
		b.append(T4);
		b.append(T5);
		b.clear();
		return b;
	}
}
