package y2.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import y2.exceptions.CardDropException;
import y2.exceptions.CardException;
import y2.exceptions.IllegalCardException;
import y2.exceptions.IllegalMoveException;
import y2.exceptions.MoveException;
import y2.exceptions.NoSuchCardException;
import y2.exceptions.TooManyCardsException;
import y2.exceptions.TooTiredToMoveException;
import y2.game.Board;
import y2.game.Tile;
import y2.game.cards.Card;
import y2.game.cards.DistractCard;
import y2.game.cards.HypnoticCard;
import y2.game.cards.PrizeCard;
import y2.game.cards.RestlessExplorerCarRestlessExplorerCard;
import y2.game.cards.RewindCard;
import y2.game.cards.StickyCard;
import y2.game.cards.VitalityCard;
import y2.game.cards.ZippyCard;
import y2.game.players.Player;

public class CardTest {

	/**
	 * Testiga kontrollitakse punkti 7a kõiki nõudeid.
	 * Kontroll hõlmab:
	 * 1) klassi PrizeCard meetodeid onPickUp() ja onDropped()
	 * läbi polümorfismi
	 * 2) klassi Card meetodit getName()
	 * 3) klassi Tile meetodeid addCard(), getCardsCount(),
	 * ja getFirstCard() 
	 * 4) klassi Player meetodeid getPoints(), pickUp(), 
	 * drop() ja getCardByName().
	 * 
	 * @throws CardException 
	 * 
	 */
	@Test
	public void testPrizeCard() 
			throws CardException {
		
		Board b = SimpleBoard.createNewBoard();
		Tile t1 = SimpleBoard.T1;
		Player p = new Player("Alice", b.getStart());
		Card c1 = new PrizeCard();
		Card c2 = new PrizeCard();
		assertTrue(c1.getName().equals("Prize"));
		
		t1.addCard(c1);
		t1.addCard(c2);					// t1-s on nüüd 2 PrizeCardi
		assertEquals(t1.getCardsCount(), 2);
		assertEquals(p.getPoints(), 0);
		p.pickUp(t1.getFirstCard());
		assertEquals(p.getPoints(), 1);
		p.pickUp(t1.getFirstCard());	// Alice'i käes nüüd 2 PrizeCardi
		assertEquals(p.getPoints(), 2);
		p.drop(c1);						// Alice kätte jäi 1 PrizeCard
		assertEquals(p.getPoints(), 1);
		p.drop(p.getCardByName("prize"));// Kätte ei jäänud ühtegi kaarti
		assertEquals(p.getPoints(), 0);
		assertEquals(t1.getCardsCount(), 2);
		
	}
	
	/**
	 * Testiga kontrollitakse kõiki punkti 7d 
	 * (VitalityCard) nõudeid ning punkti 7e 
	 * polümorfismi osa nõudeid.
	 * Kontroll hõlmab klassi VitalityCard meetodeid
	 * onPickUp() ja onDropped() läbi polümorfismi.
	 * 
	 * @throws CardException 
	 * @throws NoSuchCardException 
	 * @throws MoveException 
	 */
	@Test
	public void testVitalityCard() 
			throws NoSuchCardException, CardException, MoveException {
		
		Board b = SimpleBoard.createNewBoard();
		Player p = new Player("John", b.getStart());
		Card c = new VitalityCard();
		Tile t1 = SimpleBoard.T1;
		
		// Vaikimisi vitality ja max vitality on 3.
		assertEquals(p.getVitality(), 3);
		assertEquals(p.getMaxVitality(), 3);
		t1.addCard(c);
		p.pickUp(t1.getFirstCard());	// Mängijal nüüd 1 VitalityCard.
		assertEquals(p.getCardsCount(), 1);
	
		// Vitality ja max vitality nüüd ühe võrra suurem => 4.
		assertEquals(p.getVitality(), 4);
		assertEquals(p.getMaxVitality(), 4);
		p.forward();
		p.backward();
		assertEquals(p.getVitality(), 2);
		assertEquals(p.getMaxVitality(), 4);
		p.drop(p.getCardByName("vitality"));
		
		// Võluvägi peab haihtuma peale VitalityCard mahaviskamist.
		assertEquals(p.getVitality(), 1);
		assertEquals(p.getMaxVitality(), 3);
	}

	/**
	 * Testiga kontrollitakse punkti 7e StickyCard 
	 * nõudeid.
	 * Kontroll hõlmab klassi StickyCard meetodeid
	 * onPickUp() ja onDropped() läbi polümorfismi ning
	 * erindit CardDropException. 
	 * 
	 * @throws NoSuchCardException
	 * @throws CardException
	 */
	@Test(expected = CardDropException.class)
	public void testStickyCard() 
			throws NoSuchCardException, CardException {
		
		Board b = SimpleBoard.createNewBoard();
		Player p = new Player("Alice", b.getStart());
		Tile t1 = SimpleBoard.T1;
		t1.addCard(new StickyCard());
		
		// Mängija omaduste algoleku kontroll.
		assertEquals(p.getCardsCount(), 0);
		assertEquals(p.getPoints(), 0);
		assertEquals(p.getVitality(), 3);
		assertEquals(p.getMaxVitality(), 3);
		
		// Kontrollime mängija omadusi peale kaardi üleskorjamist.
		p.pickUp(t1.getFirstCard());
		assertEquals(p.getCardsCount(), 1);
		assertEquals(p.getPoints(), 0);
		assertEquals(p.getVitality(), 3);
		assertEquals(p.getMaxVitality(), 3);
		
		// Mahaviskamise kontroll.
		p.drop(p.getCardByName("sticky"));	// Throws CardDropException
	}
	
	/**
	 * Testiga kontrollitakse punkti 7e DistractCard 
	 * nõudeid.
	 * Kontroll hõlmab klassi DistractCard meetodeid
	 * onPickUp() ja onDropped() läbi polümorfismi.
	 * 
	 * @throws NoSuchCardException
	 * @throws CardException
	 * @throws MoveException
	 */
	@Test
	public void testDistractCard() 
			throws NoSuchCardException, CardException, MoveException {
		
		Board b = SimpleBoard.createNewBoard();
		Player p = new Player("Alice", b.getStart());
		Tile t1 = SimpleBoard.T1;
		Tile t2 = SimpleBoard.T2;
		
		// Poetame mänguväljadele kaardid.
		t1.addCard(new PrizeCard());
		t1.addCard(new PrizeCard());
		t1.addCard(new PrizeCard());	// t1-s nüüd 3 PrizeCardi.
		t2.addCard(new DistractCard());	// t2-s nüüd 1 DistractCard.
		p.pickUp(t1.getFirstCard());
		p.pickUp(t1.getFirstCard());
		p.pickUp(t1.getFirstCard());	// Mängijal nüüd 3 PrizeCardi.
		assertEquals(p.getCardsCount(), 3);
		
		// Liigume järgmisele väljale, korjame üles DistractCardi
		p.forward();	// Alice -> t2
		p.pickUp(t2.getFirstCard());	// Mängija sai DistractCardi.
		assertEquals(p.getCardsCount(), 3);
		assertTrue(p.hasCardByName("Distract"));
		assertEquals(t2.getCardsCount(), 1);
		assertEquals(t2.getFirstCard().getName(), "Prize");
										// Maha visati üks PrizeCardidest.
		
	}
	
	/**
	 * Testiga kontrollitakse punkti 9a ModifierCard 
	 * ja ActionCard toimega seotud nõudeid.
	 * 
	 * @throws NoSuchCardException
	 * @throws CardException
	 * @throws IllegalMoveException
	 * @throws TooTiredToMoveException
	 */
	@Test
	public void testModifierAndActionCardEffects() 
			throws NoSuchCardException, CardException, IllegalMoveException, TooTiredToMoveException {
		
		Board b = SimpleBoard.createNewBoard();
		Player p = new Player("Alice", b.getStart());
		Tile t2 = SimpleBoard.T2;
		Tile t3 = SimpleBoard.T3;
		Card vitality = new VitalityCard();
		Card rewind = new RewindCard();
		
		// Paigutame kaardid ja liigume kohale.
		t2.addCard(vitality);
		t3.addCard(rewind);
		p.move(t2);
		
		// Kontrollime kas VitalityCardi saab koguda ja kas
		// selle mõju kestab ainult käeshoidmise hetkel.
		assertEquals(p.getVitality(), 2);	// Algne vitaalsus
		p.pickUp(t2.getFirstCard());
		assertTrue(p.hasCard(vitality));
		assertTrue(!t2.getCards().contains(vitality));
		assertEquals(p.getVitality(), 3);	// Vitaalsus kaardi omamise hetkel
		p.drop(vitality);
		assertTrue(!p.hasCard(vitality));
		assertTrue(t2.getCards().contains(vitality));
		assertEquals(p.getVitality(), 2);	// Vitaalsus kaardi omamise hetkel
		
		// Kontrollime, kas RewindCardil on ühekordne mõju
		// ja ega seda pole võimalik koguda.
		p.move(t3);
		assertTrue(t3.containsCard(rewind));
		assertTrue(!p.hasCard(rewind));
		assertTrue(p.getLocation() != b.getStart());
		p.pickUp(t3.getFirstCard());
		
		// RewindCard pole t3-s ega mängija käes, samas
		// mängija on viidud mängulaua algusesse.
		assertTrue(!t3.containsCard(rewind));
		assertTrue(!p.hasCard(rewind));		
		assertEquals(p.getLocation(), b.getStart());
		
	}
	
	/**
	 * Testiga kontrollitakse punkti 9b RewindCardiga
	 * seotud nõudeid.
	 * 
	 * @throws NoSuchCardException
	 * @throws CardException
	 * @throws IllegalMoveException 
	 * @throws TooTiredToMoveException 
	 */
	@Test(expected = NoSuchCardException.class)
	public void testRewindCard() 
			throws NoSuchCardException, CardException, TooTiredToMoveException, IllegalMoveException {
		Board b = SimpleBoard.createNewBoard();
		Player p = new Player("Alice", b.getStart());
		Tile t2 = SimpleBoard.T2;
		Card rewind = new RewindCard();
		
		t2.addCard(rewind);
		p.forward();					// Alice -> T2
		assertEquals(p.getLocation(), t2);
		p.pickUp(t2.getFirstCard());	// Alicel nüüd RewindCard
		assertTrue(!p.hasCardByName("Rewind"));
		assertTrue(!t2.containsCards());
		assertEquals(p.getLocation(), b.getStart());
		p.drop(rewind);	// Throws NoSuchCardException
	}
	
	/**
	 * Testiga kontrollitakse punkti 9b ZippyCardiga
	 * seotud nõudeid.
	 * 
	 * @throws NoSuchCardException
	 * @throws CardException
	 */
	@Test(expected = NoSuchCardException.class)
	public void testZippyCard() 
			throws NoSuchCardException, CardException {
		
		Board b = SimpleBoard.createNewBoard();
		Player p = new Player("Bob", b.getStart());
		Tile t1 = SimpleBoard.T1;
		Card zippy = new ZippyCard();
		
		t1.addCard(zippy);
		assertEquals(p.getVitality(), 3);
		p.pickUp(t1.getFirstCard());	// Bobil nüüd ZippyCard
		assertTrue(!p.hasCardByName("Zippy"));
		assertTrue(!t1.containsCards());
		assertEquals(p.getVitality(), 4);
		p.drop(zippy);	// Throws NoSuchCardException
		
	}
	
	/**
	 * Testiga kontrollitakse punkti 9b HypnoticCardiga
	 * seotud nõudeid.
	 * @throws NoSuchCardException
	 * @throws CardException
	 */
	@Test(expected = NoSuchCardException.class)
	public void testHypnoticCard() 
			throws NoSuchCardException, CardException {
		Board b = SimpleBoard.createNewBoard();
		Player p = new Player("John", b.getStart());
		Tile t1 = SimpleBoard.T1;
		Card hypnotic = new HypnoticCard();
		
		t1.addCard(hypnotic);
		assertEquals(p.getVitality(), 3);
		p.pickUp(t1.getFirstCard());	// Johnil nüüd HypnoticCard
		assertTrue(!p.hasCardByName("Hypnotic"));
		assertTrue(!t1.containsCards());
		assertEquals(p.getVitality(), 0);
		p.drop(hypnotic);	// Throws NoSuchCardException
	}
	
	/**
	 * Testiga kontrollitakse punkti 10b nõudeid.
	 */
	@Test(expected = TooTiredToMoveException.class)
	public void testRestlessExplorerCard() 
			throws IllegalMoveException, TooTiredToMoveException,
			IllegalCardException, NoSuchCardException, 
			TooManyCardsException, CardDropException {
		
		Board b = SimpleBoard.createNewBoard();
		Player p = new Player("Bob", b.getStart());
		Tile t3 = SimpleBoard.T3;
		t3.addCard(new RestlessExplorerCarRestlessExplorerCard());
		p.move(t3);		// Bob -> t3
		assertEquals(p.getVitality(), 2);
		p.pickUp(t3.getFirstCard());	// Bobil nüüd RestlessCard
		assertTrue(p.hasCardByName("Restless"));
		for (int i = 0; i < 100; i++) {
			p.backward();	// Bob -> t2
			p.forward();	// Bob -> t3
		}
		assertEquals(p.getVitality(), 2);
		p.drop(p.getCardByName("Restless"));
		p.backward();
		p.forward();
		assertEquals(p.getVitality(), 0);
		p.backward();	// Throws TooTiredToMoveException
	}
	
}
