package y2.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import y2.exceptions.CardException;
import y2.exceptions.IllegalMoveException;
import y2.exceptions.NoSuchCardException;
import y2.exceptions.TooTiredToMoveException;
import y2.game.Board;
import y2.game.Tile;
import y2.game.cards.DispelCard;
import y2.game.cards.PrizeCard;
import y2.game.players.Player;

public class TileTest {

	/**
	 * Testiga kontrollitakse punkti 8a nõude seda osa, mille
	 * puhul ilma DispelCardita ei saa sisendeda mänguväljale
	 * ExitWonderLandTile.
	 * Kontroll hõlmab:
	 * 1) klassi Tile ja ExitWonderLandTile 
	 * meetodit canEnter()
	 * 2) klassi Player meetodit 
	 * move() 
	 * 3) erindit IllegalMoveException.
	 * 
	 * @throws IllegalMoveException
	 * @throws TooTiredToMoveException
	 * @throws OutOfBoundsMoveException
	 */
	@Test(expected = IllegalMoveException.class)
	public void testExitWithoutDispelCard() 
			throws IllegalMoveException, TooTiredToMoveException {
		Board b = SimpleBoard.createNewBoardWithExit();
		Player alice = new Player("Alice", b.getStart());
		
		alice.move(SimpleBoard.T3);		// Alice -> T3
		assertTrue(!alice.hasCardByName("Dispel"));
		alice.forward();				// Alice -> T4
		alice.forward();				// Throws IllegalMoveException
		
	}
	
	/**
	 * Testiga kontrollitakse punkti 8a ja 8b nõuete seda osa,
	 * mille puhul peab saama DispelCardiga siseneda väljakule
	 * ExitWonderLandTile.
	 * Kontroll hõlmab:
	 * 1) klassi Tile ja ExitWonderLandTile 
	 * meetodit canEnter()
	 * 2) klassi Player meetodeid
	 * move() ja getPoints()
	 * 3) klassi DispelCard meetodeid onPickUp() ja onDropped()
	 * läbi polümorfismi
	 * 
	 * @throws IllegalMoveException
	 * @throws TooTiredToMoveException
	 * @throws NoSuchCardException
	 * @throws CardException
	 */
	@Test
	public void testExitWithDispelCard() throws IllegalMoveException, TooTiredToMoveException, NoSuchCardException, CardException {
		Board b = SimpleBoard.createNewBoardWithExit();
		Player alice = new Player("Alice", b.getStart());
		Tile t3 = SimpleBoard.T3;
		Tile t4 = SimpleBoard.T4;
		
		// Paigutame T3-le 3 PrizeCardi ja T4-le ühe DispelCardi.
		t3.addCard(new PrizeCard());
		t3.addCard(new PrizeCard());
		t3.addCard(new PrizeCard());
		t4.addCard(new DispelCard());
		
		// Korjame üles kõik kaardid.
		assertEquals(alice.getPoints(), 0);
		alice.move(t3);
		alice.pickUp(t3.getFirstCard());
		alice.pickUp(t3.getFirstCard());
		alice.pickUp(t3.getFirstCard());
		alice.forward();		// Alice -> T4
		alice.pickUp(t4.getFirstCard());	// Alice sai DispelCardi
		
		// Siseneme lõpuväljakule, loeme kokku punktid.
		alice.forward();		// Alice -> T5 (Exit)
		assertEquals(alice.getPoints(), 3);
		
	}

}
