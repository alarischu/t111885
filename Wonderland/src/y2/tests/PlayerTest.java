package y2.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import y2.exceptions.CardException;
import y2.exceptions.IllegalMoveException;
import y2.exceptions.MoveException;
import y2.exceptions.NoSuchCardException;
import y2.exceptions.TooManyCardsException;
import y2.game.Board;
import y2.game.Tile;
import y2.game.cards.Card;
import y2.game.cards.DispelCard;
import y2.game.cards.PrizeCard;
import y2.game.players.FlyingPlayer;
import y2.game.players.Player;

public class PlayerTest {

	/**
	 * Testiga kontrollitakse mängijate liigutamist mängulaual.
	 * Kontroll hõlmab:
	 * 1) klassi Player meetodeid move(), forward(),
	 * backward() ja getLocation()
	 * 2) klassi Tile meetodeid getPlayersCount() ja 
	 * containsPlayer() 
	 * @throws MoveException 
	 */
	@Test
	public void testMove() throws MoveException {
		
		Board b = SimpleBoard.createNewBoard();
		Player alice = new Player("Alice", b.getStart());
		Player john = new Player("John", b.getStart());

		/* Liigutame mängijaid väljakul ringi ja kontrollime
		 * mängijate olemasolu seal. */
		assertEquals(alice.getLocation(), SimpleBoard.T1);
		alice.move(SimpleBoard.T3);	// Alice -> T3
		john.forward();				// John -> T2
		john.forward();				// John -> T3
		assertEquals(alice.getLocation(), SimpleBoard.T3);
		assertEquals(john.getLocation(), SimpleBoard.T3);
		assertEquals(SimpleBoard.T3.getPlayersCount(), 2);
		assertEquals(SimpleBoard.T1.getPlayersCount(), 0);
		assertEquals(SimpleBoard.T2.getPlayersCount(), 0);
		assertTrue(SimpleBoard.T3.containsPlayer(alice));
		assertTrue(SimpleBoard.T3.containsPlayer(john));
		alice.forward();			// Alice -> T4
		assertEquals(alice.getLocation(), SimpleBoard.T4);
		john.backward();	// John -> T2
		assertEquals(john.getLocation(), SimpleBoard.T2);
		assertEquals(SimpleBoard.T1.getPlayersCount(), 0);
		assertEquals(SimpleBoard.T2.getPlayersCount(), 1);
		assertTrue(SimpleBoard.T2.containsPlayer(john));		
		assertEquals(SimpleBoard.T3.getPlayersCount(), 0);
		assertEquals(SimpleBoard.T4.getPlayersCount(), 1);
		assertTrue(SimpleBoard.T4.containsPlayer(alice));
	}
	
	/**
	 * Testiga kontrollitakse mängijate liigutamist mängulaual
	 * väljaspoole mängulaua piire. Kontroll hõlmab klassi
	 * Player meetodeid move(), forward() ning erindit
	 * IllegalMoveException.
	 * @throws IllegalMoveException 
	 */
	@Test(expected = IllegalMoveException.class)
	public void testMoveExceptions() throws MoveException {
		
		Board b = SimpleBoard.createNewBoard();
		Player alice = new Player("Alice", b.getStart());
		alice.move(SimpleBoard.T4);
		alice.forward();	// Throws OutOfBoundsMoveException
	}
	
	/**
	 * Testiga kontrollitakse punkti 7b kõiki nõudeid. 
	 * Kontroll hõlmab Player klassi meetodeid pickUp(), 
	 * drop(), getCards() ja getCardByName() ning erindit
	 * CardException.
	 * @throws NoSuchCardException
	 * @throws CardException
	 * @throws MoveException 
	 */
	@Test(expected = CardException.class)
	public void testMaxCardsException() throws NoSuchCardException, CardException, MoveException {
		
		 Board b = SimpleBoard.createNewBoard();
		 Tile t1 = SimpleBoard.T1;
		 Tile t2 = SimpleBoard.T2;
		 Player p = new Player("John", b.getStart());
		 
		 // Paigutame mänguväljakule kaardid.
		 for (int i = 0; i < 7; i++) {
			 t1.addCard(new PrizeCard());
		 } 		 // t1-s on nüüd 7 kaarti
		 assertTrue(t1.containsCards());
		 assertEquals(t1.getCardsCount(), 7);

		 // Korjame osa kaarte üles.
		 p.pickUp(t1.getFirstCard());
		 p.pickUp(t1.getFirstCard());
		 p.pickUp(t1.getFirstCard());
		 p.pickUp(t1.getFirstCard());
		 p.pickUp(t1.getFirstCard());	// Mängija käes nüüd 5 kaarti
		 assertEquals(t1.getCardsCount(), 2);
		 List<Card> cards = p.getCards();
		 assertEquals(cards.size(), 5);
		 
		 // Liigume teisele mänguväljale ja viskame ühe kaardi maha.
		 p.forward();	// John -> t2
		 p.drop(p.getCardByName("prize"));	// Viskame ühe kaardi maha
		 assertEquals(t2.getCardsCount(), 1);
		 assertEquals(p.getCardsCount(), 4);	// Mängija käes 4 kaarti
		 p.backward();
		 p.pickUp(t1.getFirstCard());	// Mängija käes nüüd 5 kaarti
		 assertEquals(p.getCardsCount(), 5);
		 assertEquals(t1.getCardsCount(), 1);
		 p.pickUp(t1.getFirstCard());	// Throws CardException
	 
	}
	
	/**
	 * Testiga kontrollitakse punkti 7c kõiki nõudeid. 
	 * Kontroll hõlmab Player klassi meetodeid getVitality()
	 * ja sleep() ning erindit MoveException.
	 * @throws MoveException
	 */
	@Test(expected = MoveException.class)
	public void testVitality() throws MoveException {
		
		 Board b = SimpleBoard.createNewBoard();
		 Player p = new Player("Alice", b.getStart());
		 
		 assertEquals(p.getVitality(), 3);
		 p.forward();
		 p.backward();
		 p.forward();	// Jaks nulli.
		 assertEquals(p.getVitality(), 0);
		 p.sleep();		// Mängija jaksu taastamine.
		 assertEquals(p.getVitality(), 3);
		 p.forward();
		 p.backward();
		 p.forward();	// Jaks nulli.
		 p.backward();	// Throws MoveException
	
	}
	
	/**
	 * Testiga kontrollitakse punkti 10c nõudeid.
	 * @throws MoveException
	 * @throws CardException
	 */
	@Test(expected = TooManyCardsException.class)
	public void testFlyingPlayer() 
			throws MoveException, CardException {
		 Board b = SimpleBoard.createNewBoard();
		 Player p = new FlyingPlayer("Flyer", b.getStart());
		 Tile t3 = SimpleBoard.T3;
		 t3.addCard(new PrizeCard());
		 t3.addCard(new PrizeCard());
		 t3.addCard(new DispelCard());
		 for (int i = 0; i < 100; i++) {
			 p.forward();
			 p.backward();
		 }
		 p.move(t3);
		 p.pickUp(t3.getFirstCard());
		 p.pickUp(t3.getFirstCard());
		 assertEquals(p.getCardsCount(), 2);
		 p.pickUp(t3.getFirstCard());	// Throws TooManyCardsException
		 
	}

}
