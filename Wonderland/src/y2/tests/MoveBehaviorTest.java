package y2.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import y2.exceptions.MoveException;
import y2.exceptions.TooTiredToMoveException;
import y2.game.Board;
import y2.game.players.Player;
import y2.game.players.UnlimitedMoves;

public class MoveBehaviorTest {

	/**
	 * Käesoleva testiga kontrollitakse punkti 10a 
	 * MoveBehavior ja NormalMoves nõudeid.
	 * Kontrolli käigus testitakse klassi NormalMoves
	 * mõju mängija käitumisele: mängija peab saama käia
	 * max. 3 korda, peale mida tuleb rakendada meetodit
	 * sleep(), et jaksu taastada. Kui mängija jaks on 
	 * nullis ja üritatakse edasi liikuda, heidetakse 
	 * TooTiredToMoveException erind.
	 * @throws MoveException
	 */
	@Test(expected = TooTiredToMoveException.class)
	public void testNormalMoves() 
			throws MoveException {
		
		Board b = SimpleBoard.createNewBoard();
		Player alice = new Player("Alice", b.getStart());
		assertEquals(alice.getVitality(), 3);
		alice.forward();
		alice.backward();
		alice.forward();
		assertEquals(alice.getVitality(), 0);	// Jaoks nullis
		alice.sleep();
		assertEquals(alice.getVitality(), 3);
		alice.forward();
		alice.backward();
		alice.forward();
		alice.backward();	// Throws TooTiredToMoveException
		
	}
	
	/**
	 * Käesoleva testiga kontrollitakse punkti 10a 
	 * MoveBehavior ja UnlimitedMoves nõudeid. 
	 * UnlimitedMoves rakendav mängija ei kasuta sel puhul
	 * vitaalsust (ei väsi) ning saab käia lõputult
	 * mööda mängulauda.
	 * @throws MoveException
	 */
	@Test
	public void testUnlimitedMoves() 
			throws MoveException {
		
		Board b = SimpleBoard.createNewBoard();
		Player john = new Player("John", b.getStart());
		john.setBehavior(new UnlimitedMoves());
		assertEquals(john.getVitality(), 3);
		for (int i = 0; i < 100; i++) {
			john.forward();
			john.backward();
		}
		assertEquals(john.getVitality(), 3);
		john.sleep();
		assertEquals(john.getVitality(), 3);
	}

}
