package y2.game;

import y2.game.players.Player;

/**
 * Mänguväljaku (Tile) alamliik, mille kaudu pääseb 
 * mängija Imedemaalt välja ja saab mängu ära lõpetada.
 * @author alari
 *
 */
public class ExitWonderLandTile extends Tile {

	public ExitWonderLandTile(String name) {
		super(name);

	}

	/**
	 * Mängija võib sisenda ExitWonderLandTile mänguväljale
	 * kui tal on korjatud Dispel kaart.
	 */
	@Override
	public boolean canEnter(Player player) {
		
		return player.hasCardByName("Dispel");
	}

}
