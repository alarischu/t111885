package y2.game.cards;

import y2.exceptions.CardDropException;
import y2.exceptions.IllegalCardException;
import y2.exceptions.NoSuchCardException;
import y2.exceptions.TooManyCardsException;
import y2.game.players.Player;

/**
 * ActionCard on Card klassi alamliik, mis annab kaartidele ühekordse 
 * toime ning milliseid kaarte mängija koguda ei saa, sest peale 
 * toime rakendumist kaob kaart mängust.
 * @author alari
 *
 */
public class ActionCard extends Card {

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onPickedUp(Player owner) 
			throws IllegalCardException, NoSuchCardException, 
			TooManyCardsException {
		
		owner.getLocation().getCards().remove(this);
		
	}

	@Override
	public void onDropped(Player owner) 
			throws CardDropException {
		
		throw new CardDropException("Sellist kaarti ei saa maha visata");
		
	}

}
