package y2.game.cards;

import y2.exceptions.CardDropException;
import y2.exceptions.IllegalCardException;
import y2.exceptions.NoSuchCardException;
import y2.exceptions.TooManyCardsException;
import y2.game.players.Player;

/**
 * Mängukaart (Card) on mänguväljal paiknev asi, mida mängija saab 
 * üles korjata. Igal mängukaardil on nimi ja võluvõime, mis
 * mõjutab mängijat.
 * @author alari
 *
 */
public abstract class Card {

	/**
	 * Meetod mängukaardi nime saamiseks.
	 * Meetod on implementeeritud alamklassis.
	 * @return Mängukaardi nimi.
	 */
	public abstract String getName();
	
	/**
	 * Meetod, mis rakendub mängukaardi üleskorjamisel.
	 * Meetod on implementeeritud alamklassis.
	 * @param owner Mängija, kes kaardi üles korjas.
	 * @throws IllegalCardException
	 * @throws NoSuchCardException
	 * @throws TooManyCardsException
	 */
	public abstract void onPickedUp(Player owner) 
			throws IllegalCardException, NoSuchCardException, 
			TooManyCardsException;
	
	/**
	 * Meetod, mis rakendub mängukaardi mahaviskamisel.
	 * Meetod on implementeeritud alamklassis.
	 * @param owner Mängija, kes kaardi maha viskas.
	 * @throws CardDropException
	 */
	public abstract void onDropped(Player owner) 
			throws CardDropException;
	
	@Override
	public String toString() {
		return getName();
	}
}
