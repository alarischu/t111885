package y2.game.cards;

import y2.exceptions.CardDropException;
import y2.exceptions.IllegalCardException;
import y2.exceptions.NoSuchCardException;
import y2.exceptions.TooManyCardsException;
import y2.game.players.NormalMoves;
import y2.game.players.Player;
import y2.game.players.UnlimitedMoves;

/**
 * RestlessExplorerCarRestlessExplorerCard on kaart, mille 
 * üles korjanud mängija ei väsi ära ning seetõttu ei pea ta 
 * kunagi magama, et kadunud vitaalsust taastada. Mõju
 * kestab seni, kuni kaart on mängija käes.
 * @author alari
 *
 */
public class RestlessExplorerCarRestlessExplorerCard 
extends ModifierCard {
	
	@Override
	public String getName() {
		return "Restless";
	}
	
	@Override
	public void onPickedUp(Player owner) 
			throws IllegalCardException, NoSuchCardException, TooManyCardsException {
		
		super.onPickedUp(owner);
		owner.setBehavior(new UnlimitedMoves());
		
	}

	@Override
	public void onDropped(Player owner) throws CardDropException {
		
		super.onDropped(owner);
		owner.setBehavior(new NormalMoves());
		
	}

}
