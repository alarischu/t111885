package y2.game.cards;

import java.util.List;
import java.util.Random;

import y2.exceptions.CardDropException;
import y2.exceptions.IllegalCardException;
import y2.exceptions.NoSuchCardException;
import y2.exceptions.TooManyCardsException;
import y2.game.players.Player;

/**
 * DistractCard on kaart, mille üles korjamisel poetab mängija 
 * hajameelsusest maha ühe oma olemasolevatest kaartidest.
 * @author alari
 *
 */
public class DistractCard extends ModifierCard {

	@Override
	public void onPickedUp(Player owner) 
			throws IllegalCardException, NoSuchCardException, TooManyCardsException {
		
		super.onPickedUp(owner);
		List<Card> cards = owner.getCards();
		if (cards.size() > 1) {
			/* Vali mahavisatav kaart juhuslikult varem käes
			 * olevate kaartide seast. */
			Random random = new Random();
			int randIndex = random.nextInt(cards.size() - 1);
			try {
				owner.drop(cards.get(randIndex));
			} catch (CardDropException e) {
				// TODO Auto-generated catch block
				return;
			}
		}
	}

	@Override
	public void onDropped(Player owner) throws CardDropException {
		
		super.onDropped(owner);
		
	}

	@Override
	public String getName() {
		return "Distract";
	}

}
