package y2.game.cards;

import y2.exceptions.CardDropException;
import y2.exceptions.IllegalCardException;
import y2.exceptions.NoSuchCardException;
import y2.exceptions.TooManyCardsException;
import y2.game.players.Player;

/**
 * StickyCard on kaart, mis ei anna mängijale ühtegi 
 * kasulikku omadust, aga mida ei ole võimalik maha visata.
 * @author alari
 *
 */
public class StickyCard extends ModifierCard {

	@Override
	public void onPickedUp(Player owner) 
			throws IllegalCardException, NoSuchCardException, 
			TooManyCardsException { 
		
		super.onPickedUp(owner);
	}

	@Override
	public void onDropped(Player owner) 
			throws CardDropException {
		
		super.onDropped(owner);
		throw new CardDropException("Ei saa kaarti maha visata");
		
	}

	@Override
	public String getName() {
		return "Sticky";
	}

}
