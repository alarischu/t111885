package y2.game.cards;

import y2.exceptions.IllegalCardException;
import y2.exceptions.NoSuchCardException;
import y2.exceptions.TooManyCardsException;
import y2.game.players.Player;

/**
 * ZippyCard on kaart, mis annab mängijale 
 * püsivalt 1 pügala vitaalsust juurde.
 * @author alari
 *
 */
public class ZippyCard extends ActionCard {
	
	@Override
	public String getName() {
		return "Zippy";
	}

	@Override
	public void onPickedUp(Player owner) 
			throws IllegalCardException, NoSuchCardException, 
			TooManyCardsException {
		
		super.onPickedUp(owner);
		owner.setVitality(owner.getVitality() + 1);
		
	}

}
