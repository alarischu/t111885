package y2.game.cards;

import java.util.List;

import y2.exceptions.CardDropException;
import y2.exceptions.IllegalCardException;
import y2.exceptions.NoSuchCardException;
import y2.exceptions.TooManyCardsException;
import y2.game.players.Player;

/**
 * ModifierCard on Card klassi alamklass, mis annab kaartidele
 * toime seniks, kuni kaart on mängija käes. 
 * @author alari
 *
 */
public class ModifierCard extends Card {

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onPickedUp(Player owner) 
			throws IllegalCardException, NoSuchCardException, TooManyCardsException {
		
		List<Card> tileCards = owner.getLocation().getCards();
		tileCards.remove(this);
		owner.getCards().add(this);
		
	}

	@Override
	public void onDropped(Player owner) throws CardDropException {
		
		owner.getCards().remove(this);
		owner.getLocation().addCard(this);
		
	}

}
