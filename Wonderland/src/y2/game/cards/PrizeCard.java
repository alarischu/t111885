package y2.game.cards;

import y2.exceptions.CardDropException;
import y2.exceptions.IllegalCardException;
import y2.exceptions.NoSuchCardException;
import y2.exceptions.TooManyCardsException;
import y2.game.players.Player;

/**
 * PrizeCard on mängukaart, mis annab mängijale 
 * juurde 1 punkti.
 * @author alari
 *
 */
public class PrizeCard extends ModifierCard {

	@Override
	public void onPickedUp(Player owner) 
			throws IllegalCardException, NoSuchCardException, 
			TooManyCardsException {
		
		super.onPickedUp(owner);
		owner.setPoints(owner.getPoints() + 1);
	}

	@Override
	public void onDropped(Player owner) 
			throws CardDropException {
		
		super.onDropped(owner);
		owner.setPoints(owner.getPoints() - 1);
		
	}

	protected int getExtraPoints() {
		return 1;
	}

	@Override
	public String getName() {
		return "Prize";
	}
}
