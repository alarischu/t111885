package y2.game.cards;

import y2.exceptions.IllegalCardException;
import y2.exceptions.NoSuchCardException;
import y2.exceptions.TooManyCardsException;
import y2.game.players.Player;

/**
 * RewindCard on kaart, mille üles korjamisel viiakse 
 * mängija tagasi mängulaua algusesse.
 * @author alari
 *
 */
public class RewindCard extends ActionCard {
	
	@Override
	public String getName() {
		return "Rewind";
	}
	
	@Override
	public void onPickedUp(Player owner) 
			throws IllegalCardException, NoSuchCardException, 
			TooManyCardsException {
		
		super.onPickedUp(owner);
		owner.setLocation(owner.getLocation().getBoard().getStart());
		
		
	}

}
