package y2.game.cards;

import y2.exceptions.IllegalCardException;
import y2.exceptions.NoSuchCardException;
import y2.exceptions.TooManyCardsException;
import y2.game.players.Player;

/**
 * HypnoticCard on kaart, mille üles noppimisel muutub 
 * mängija uniseks ning kaotab kogu oma vitaalsuse.
 * @author alari
 *
 */
public class HypnoticCard extends ActionCard {

	@Override
	public String getName() {
		return "Hypnotic";
	}

	@Override
	public void onPickedUp(Player owner) 
			throws IllegalCardException, NoSuchCardException, 
			TooManyCardsException {
		
		super.onPickedUp(owner);
		owner.setVitality(getHypnoticVitality());
		
	}
	
	protected int getHypnoticVitality() {
		return 0;
	}
	
}
