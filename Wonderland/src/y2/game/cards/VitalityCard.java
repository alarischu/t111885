package y2.game.cards;

import y2.exceptions.CardDropException;
import y2.exceptions.IllegalCardException;
import y2.exceptions.NoSuchCardException;
import y2.exceptions.TooManyCardsException;
import y2.game.players.Player;

/**
 * VitalityCard on kaart, mis annab mängijale 
 * juurde 1 pügala vitaalsust ja maksimaalset
 * vitaalsust. Mõju kestab seni, kuni kaart
 * on käes.
 * @author alari
 *
 */
public class VitalityCard extends ModifierCard {

	@Override
	public void onPickedUp(Player owner) 
			throws IllegalCardException, NoSuchCardException, 
			TooManyCardsException {
		
		super.onPickedUp(owner);
		owner.setVitality(owner.getVitality() + 1);
		owner.setMaxVitality(owner.getMaxVitality() + 1);
		
	}

	@Override
	public void onDropped(Player owner) 
			throws CardDropException {
		
		super.onDropped(owner);
		owner.setVitality(owner.getVitality() - 1);
		owner.setMaxVitality(owner.getMaxVitality() - 1);
		
	}

	@Override
	public String getName() {
		return "Vitality";
	}

}
