package y2.game.cards;

import y2.exceptions.CardDropException;
import y2.exceptions.IllegalCardException;
import y2.exceptions.NoSuchCardException;
import y2.exceptions.TooManyCardsException;
import y2.game.players.Player;

/**
 * DispelCard on kaart, mis annab selle üleskorjamisel 
 * mängijale õiguse siseneda mänguväljale ExitWonderLandTile.
 * @author alari
 *
 */
public class DispelCard extends ModifierCard {

	@Override
	public String getName() {
		return "Dispel";
	}

	@Override
	public void onPickedUp(Player owner) 
			throws IllegalCardException, NoSuchCardException, 
			TooManyCardsException  {
		
		super.onPickedUp(owner);
		
	}

	@Override
	public void onDropped(Player owner) throws CardDropException {
		
		super.onDropped(owner);
		
	}

}
