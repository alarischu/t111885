package y2.game;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import y2.exceptions.NoSuchCardException;
import y2.game.cards.Card;
import y2.game.players.Player;

/**
 * Mängulaua mänguväli.
 * @author alari
 *
 */
public class Tile {

	/**
	 * Mänguvälja nimi.
	 */
	private String name;
	
	/**
	 * Viit eelmisele mänguväljale.
	 */
	private Tile previous;
	
	/**
	 * Viit järgmisele mänguväljale.
	 */
	private Tile next;
	
	/**
	 * Mängulaud, milles mänguväli sisaldub.
	 */
	private Board board;
	
	/**
	 * Andmestruktuur mänguväljal paiknevate mängijate
	 * jaoks.
	 */
	private List<Player> players = new ArrayList<Player>();
	
	/**
	 * Andmestruktuur mänguväljakul paiknevate kaartide
	 * jaoks.
	 */
	private List<Card> cards = new LinkedList<Card>();
	
	/**
	 * Klassi konstruktor.
	 * @param name Mänguvälja nimi.
	 */
	public Tile(String name) {
		this.name = name;
	}
	
	/**
	 * Meetod mänguvälja nime saamiseks.
	 * @return Mänguvälja nimi.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Meetod mängulaua saamiseks, milles
	 * mänguväli sisaldub.
	 * @return Mängulaud, milles mänguväli sisaldub.
	 */
	public Board getBoard() {
		return board;
	}
	
	/**
	 * Meetod mängulaua määramiseks, milles mänguväli
	 * sisaldub.
	 * @param board Mängulaud, milles mänguväli
	 * sisaldub.
	 */
	void setBoard(Board board) {
		this.board = board;
	}

	/**
	 * Meetod eelmise mänguvälja saamiseks.
	 * @return Käesolevast mänguväljast tagapool
	 * paiknev mänguväli.
	 */
	public Tile getPrevious() {
		return this.previous;
	}
	
	/**
	 * Meetod eelmise mänguvälja määramiseks.
	 * @param previous Käesolevast mänguväljast 
	 * tagapool paiknev mänguväli.
	 */
	void setPrevious(Tile previous) {
		this.previous = previous;
	}
	
	/**
	 * Meetod järgmise mänguvälja saamiseks.
	 * @return Käesolevast mänguväljast eespool
	 * paiknev mänguväli.
	 */
	public Tile getNext() {
		return this.next;
	}
	
	/**
	 * Meetod järgmise mänguvälja määramiseks.
	 * @param next Käesolevast mänguväljast 
	 * eespool paiknev mänguväli.
	 */
	void setNext(Tile next) {
		this.next = next;
	}
	
	/**
	 * Meetod mänguväljal paiknevate kaartide saamiseks.
	 * @return Nimekiri mänguväljal paiknevatest kaartidest.
	 */
	public List<Card> getCards() {
		return cards;
	}
	
	/**
	 * Meetod mänguväljal paiknevate kaartide
	 * arvu saamiseks.
	 * @return Mänguväljal paiknevate kaartide arv.
	 */
	public int getCardsCount() {
		return cards.size();
	}
	
	/**
	 * Meetod mänguvälja kaartidest esimese saamiseks.
	 * @return Esimene kaart mänguväljal paiknevate
	 * kaartide loetelust.
	 * @throws NoSuchCardException Kui kaarti ei leita.
	 */
	public Card getFirstCard() 
			throws NoSuchCardException {
		
		Card card;
		try {
			card = ((LinkedList<Card>) getCards()).getFirst();
		}
		catch (NoSuchElementException e) {
			throw new NoSuchCardException("Sellist kaarti ei ole");
		}
		return card;
	}
	
	/**
	 * Meetod kaardi lisamiseks mänguväljale.
	 * @param card Lisatav kaart.
	 * @return True, kui selline kaart on juba olemas.
	 */
	public boolean addCard(Card card) {
		return cards.add(card);
	}
	
	/**
	 * Meetod kaartide olemasolu kontrolliks mänguväljal.
	 * @return True, kui käesoleval mänguväljal leidub 
	 * vähemalt 1 kaart.
	 */
	public boolean containsCards() {
		return getCardsCount() > 0;
	}
	
	/**
	 * Meetod kaardi olemasolu kontrolliks mänguväljal.
	 * @param card Otsitav kaart.
	 * @return True, kui käesoleval mänguväljal leidub 
	 * otsitav kaart.
	 */
	public boolean containsCard(Card card) {
		return getCards().contains(card);
	}

	/**
	 * Meetod mänguväljal paiknevate mängijate
	 * arvu saamiseks.
	 * @return Käesoleval mänguväljal paiknevate 
	 * mängijate arv.
	 */
	public int getPlayersCount() {
		return players.size();
	}
	
	/**
	 * Meetod mängija lisamiseks mänguväljale.
	 * @param player Lisatav mängija.
	 * @return True, kui selline mängija on juba 
	 * mänguväljal olemas.
	 */
	public boolean addPlayer(Player player) {
		return players.add(player);
	}
	
	/**
	 * Meetod mängija eemaldamiseks mänguväljalt.
	 * @param player Eemaldatav mängija.
	 * @return True, kui eemaldatav mängija oli
	 * mänguväljal olemas.
	 */
	public boolean removePlayer(Player player) {
		return players.remove(player);
	}
	
	/**
	 * Meetod mängija olemasolu kontrolliks mänguväljal.
	 * @param player Otsitav mängija.
	 * @return True, kui käesoleval mänguväljal leidus
	 * otsitav mängija.
	 */
	public boolean containsPlayer(Player player) {
		return players.contains(player);
	}
	
	/**
	 * Meetod kõigi mänguväljal paiknevate mängijate 
	 * ja kaartide eemaldamiseks.
	 */
	public void clear() {
		cards.clear();
		players.clear();
	}

	/**
	 * Meetod mänguväljale sisenemise kontrollimiseks.
	 * @param player Mänguväljale siseneda sooviv mängija.
	 * @return True, kui mängijal on õigus mänguväljale 
	 * siseneda.
	 */
	public boolean canEnter(Player player) {
		return true;
	}
	
	@Override
	public String toString() {
		return name;
	}

}
