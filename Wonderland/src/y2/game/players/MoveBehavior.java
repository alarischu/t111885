package y2.game.players;

import y2.exceptions.IllegalMoveException;
import y2.exceptions.TooTiredToMoveException;
import y2.game.Tile;

/**
 * MoveBehavior on mängija (Player) liikumist kui käitumist
 * kirjeldav baasklass.
 * @author alari
 *
 */
public abstract class MoveBehavior {

	/**
	 * Alamklassides on implementeeritud liikumisega seotud
	 * tegevused.
	 * @param owner Mängija, kes soovib liikuda.
	 * @param tile Mänguväli, kuhu mängija soovib liikuda.
	 * @throws IllegalMoveException Kui tekib viga mänguväljale 
	 * liikumisel.
	 * @throws TooTiredToMoveException Kui mängija vitaalsus on 
	 * enne liikuma hakkamist nullis.
	 */
	protected abstract void performMove(Player owner, Tile tile)
			throws IllegalMoveException, TooTiredToMoveException;
	
	/**
	 * Alamklassides on implementeeritud magamisega seotud 
	 * tegevused.
	 * @param owner Mängija, kelle peal rakendatakse magamist.
	 */
	protected abstract void performSleep(Player owner);
	
	/**
	 * Meetod mängija liigutamiseks järgmisele mänguväljale.
	 * @param owner Mängija, kes soovib sisenda järgmisele
	 * mänguväljale.
	 * @throws TooTiredToMoveException Kui mängija jaks on nullis.
	 * @throws IllegalMoveException Kui mängija ei saa sisendeda 
	 * mänguväljale.
	 */
	protected void performForward(Player owner) 
			throws TooTiredToMoveException, IllegalMoveException {
		
		Tile next = owner.getLocation().getNext();
		performMove(owner, next);
	}
	
	/**
	 * Meetod mängija liigutamiseks eelmisele mänguväljale.
	 * @param owner Mängija, kes soovib sisenda eelmisele
	 * mänguväljale.
	 * @throws TooTiredToMoveException Kui mängija jaks on nullis.
	 * @throws IllegalMoveException Kui mängija ei saa sisendeda 
	 * mänguväljale.
	 */
	protected void performBackward(Player owner) 
			throws TooTiredToMoveException, IllegalMoveException  {
		
		Tile previous = owner.getLocation().getPrevious();
		performMove(owner, previous);
	}
	
	/**
	 * Meetod mängija registreerimiseks uuele, sisenetavale mänguväljale
	 * ning väljaregistreerimiseks eelmiselt mänguväljalt.
	 * @param owner Mängija, kes mänguväljade vahel liigub.
	 * @param newLocation Uus mänguväli, kuhu mängija liigub.
	 */
	protected void registerPlayer(Player owner, Tile newLocation) {
		
		owner.getLocation().removePlayer(owner);
		owner.setLocation(newLocation);
		newLocation.addPlayer(owner);
	}

	
}
