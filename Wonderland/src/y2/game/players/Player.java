package y2.game.players;

import java.util.ArrayList;
import java.util.List;

import y2.exceptions.*;
import y2.game.Tile;
import y2.game.cards.Card;

/**
 * Imedemaa (Wonderland) mängus osalev mängija.
 * @author alari
 *
 */
public class Player {

	/**
	 * Mänguväli, millel mängija paikneb.
	 */
	private Tile location;
	
	/**
	 * Mängija nimi.
	 */
	private String name;
	
	/**
	 * Mängija liikumist iseloomustav käitumine.
	 */
	private MoveBehavior behavior = new NormalMoves();
	
	/**
	 * Andmestruktuur mängija käes olevate kaartiga. 
	 */
	private List<Card> cards = new ArrayList<Card>();
	
	/**
	 * Mängija kogutud punktide arv.
	 */
	private int points = 0;
	
	/**
	 * Mängija vitaalsus.
	 */
	private int vitality;
	
	/**
	 * Mängija maksimaalne võimalik vitaalsus.
	 */
	private int maxVitality;
	
	
	/**
	 * Klassi konstruktor.
	 * @param name Mängija nimi.
	 * @param start Mänguväli, kuhu mängija 
	 * loomisel paigutatakse.
	 */
	public Player(String name, Tile start) {
		setName(name);
		setLocation(start);
		setBehavior(new NormalMoves());
		setVitality( ((NormalMoves)getBehavior()).getInitialVitality() );
		setMaxVitality( ((NormalMoves)getBehavior()).getInitialVitality() );
				
	}

	/**
	 * Meetod mängija liikumist iseloomustava käitumise
	 * saamiseks.
	 * @return
	 */
	public MoveBehavior getBehavior() {
		
		return behavior;
	}
	
	/**
	 * Meetod mängija liikumist iseloomustava käitumise
	 * määramiseks.
	 * @param behavior Mängija liikumise käitumine.
	 */
	public void setBehavior(MoveBehavior behavior) {
		
		this.behavior = behavior;
	}
	
	/**
	 * Meetod maksimaalse käes hoitavate kaartide 
	 * arvu saamiseks.
	 * @return Max. kaartide arv, mida mängija käes
	 * saab hoida.
	 */
	protected int getMaxCardsAllowed() {
		return 5;
	}
	
	/**
	 * Meetod mängija asukoha saamiseks.
	 * @return Mänguväli, millel mängija paikneb.
	 */
	public Tile getLocation() {
		return location;
	}

	/**
	 * Meetod mängija asukoha määramiseks.
	 * @param location Mänguväli, millele mängija
	 * paigutatakse.
	 */
	public void setLocation(Tile location) {
		this.location = location;
	}
	
	/**
	 * Meetod mängija nime saamiseks.
	 * @return Mängija nimi.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Meetod mängija nime määramiseks.
	 * @param name Mängijale määratav nimi.
	 * @throws IllegalArgumentException Kui nimi on
	 * tühi string või null.
	 */
	protected void setName(String name) 
			throws IllegalArgumentException {
		if (name == null || name.isEmpty()) {
			throw new IllegalArgumentException(
					"Nimi ei tohi olla tühi ega null!");
		}
		this.name = name;
	}
	
	/**
	 * Meetod mängija kogutud punktide arvu saamiseks.
	 * @return Mängija kogutud punktide arv.
	 */
	public int getPoints() {
		return points;
	}

	/**
	 * Meetod mängija kogutud punktide arvu määramiseks.
	 * @param points Mängija punktide arv.
	 */
	public void setPoints(int points) {
		this.points = points;
	}

	/**
	 * Meetod mängija vitaalsuse saamiseks.
	 * @return Mängija vitaalsus.
	 */
	public int getVitality() {
		return vitality;
	}

	/**
	 * Meetod mängija vitaalsuse määramiseks.
	 * @param vitality Vitaalsuse määr.
	 */
	public void setVitality(int vitality) {
				
		this.vitality = vitality;
	}
	
	/**
	 * Meetod mängija maksimaalse vitaalsuse saamiseks.
	 * @return Maksimaalne võimalik vitaalsus, mida mängijal
	 * on võimalik saavutada.
	 */
	public int getMaxVitality() {
		
		return maxVitality;
	}
	
	/**
	 * Meetod mängija maksimaalse vitaalsuse määramiseks.
	 * @param maxVitality Maksimaalse vitaalsuse määr.
	 */
	public void setMaxVitality(int maxVitality) {
		this.maxVitality = maxVitality;
	}
	
	/**
	 * Meetod mängija käes olevate kaartide saamiseks.
	 * @return Nimekiri mängija käes olevatest kaartidest.
	 */
	public List<Card> getCards() {
		return cards;
	}
	
	/**
	 * Meetod mängija käes olevate kaartide arvu saamiseks.
	 * @return Mängija käes olevate kaartide arv.
	 */
	public int getCardsCount() {
		return getCards().size();
	}

	/**
	 * Meetod mängija käes oleva kaardi olemasolu kontrolliks.
	 * @param card Otsitav kaart.
	 * @return True, kui mängija käes on otsitav kaart.
	 */
	public boolean hasCard(Card card) {
		return getCards().contains(card);
	}
	
	/**
	 * Meetod mängija käes oleva kaardi olemasolu kontrolliks
	 * kaardi nime järgi.
	 * @param name Otsitava kaardi nimi (nt. "Prize").
	 * @return True, kui mängija käes on otsitava nimega
	 * kaart.
	 */
	public boolean hasCardByName(String name) {
		try {
			getCardByName(name);
			return true;
		} catch (NoSuchCardException e) {
			return false;
		}
		
	}

	/**
	 * Meetod mängija käes oleva kaardi saamiseks kaardi nime
	 * järgi.
	 * @param name Kaardi nimi.
	 * @return Esimene kaart nimekirjas, mis on mängijal olemas ning
	 * mille nimetus vastab kaardile.
	 * @throws NoSuchCardException Kui vastava nimetusega kaarti ei
	 * leita.
	 */
	public Card getCardByName(String name) 
			throws NoSuchCardException {
		
		for (Card card : cards) {
			if (card.getName().equalsIgnoreCase(name)) {
				return card;
			}
		}
		
		throw new NoSuchCardException("Kaarti nimega " + name + " ei leitud");
	}
	
	
	/**
	 * Meetod kaardi üles korjamiseks mänguväljalt, millel mängija
	 * paikneb. Implementeeritud Card klassis.
	 * @param card Kaart, mida soovitakse üles korjata.
	 * @throws IllegalCardException Kui kaart on null.
	 * @throws NoSuchCardException Kui selline kaart mänguväljal puudub.
	 * @throws TooManyCardsException Kui kätte ei mahu rohkem kaarte.
	 */
	public void pickUp(Card card) 
			throws IllegalCardException, NoSuchCardException, 
			TooManyCardsException 
			 
			 {
		
		List<Card> tileCards = getLocation().getCards();
		
		if (card == null) {
			throw new IllegalCardException("Kaart ei tohi olla null");
		}
		
		if (!tileCards.contains(card)) {
			throw new NoSuchCardException("Ei saa üles korjata - selline " +
					"kaart puudub");
		}
		
		if (getCardsCount() >= getMaxCardsAllowed()) {
			throw new TooManyCardsException("Ei mahu rohkem kaarte - max. " +
					"kaartide arv " + getMaxCardsAllowed() + " täis");
		}
		
		card.onPickedUp(this);
	}
	
	/**
	 * Meetod kaardi maha viskamiseks mänguväljale, millel mängija
	 * paikneb. Implementeeritud Card klassis.
	 * @param card Kaart, mida soovitakse maha visata.
	 * @throws IllegalCardException Kui kaart on null.
	 * @throws NoSuchCardException Kui selline kaart mängija käes puudub.
	 * @throws CardDropException Kui ei ole ühtegi kaarti, mida maha visata.
	 */
	public void drop(Card card) 
			throws IllegalCardException, NoSuchCardException, 
			CardDropException {
		
		if (card == null) {
			throw new IllegalCardException("Kaart ei tohi olla null");
		}
		
		if (!getCards().contains(card)) {
			throw new NoSuchCardException("Ei saa maha visata - " +
					"selline kaart puudub");
		}
		
		if (getCardsCount() < 1) {
			throw new CardDropException("Ei ole ühtegi kaarti, mida " +
					"maha visata");
		}
		
		card.onDropped(this);
	}
	
	/**
	 * Meetod mängija liigutamiseks mängulaual. Implementeeritud 
	 * MoveMehavior klassis.
	 * @param tile Mänguväli, kuhu mängijat soovitakse liigutada.
	 * @throws IllegalMoveException Kui mänguväli on null või kui mängija
	 * ei saa mänguväljale siseneda.
	 * @throws TooTiredToMoveException Kui mängija vitaalsus on nullis.
	 */
	public void move(Tile tile) 
			throws IllegalMoveException, TooTiredToMoveException {
		
		getBehavior().performMove(this, tile);
	}
	
	/**
	 * Meetod mängija mängulaual liigutamiseks järgmisele 
	 * mänguväljale. Implementeeritud MoveMehavior klassis.
	 * @throws TooTiredToMoveException Kui mängija vitaalsus on 0.
	 * @throws IllegalMoveException Kui mängija ei saa siseneda
	 * mänguväljale või kui mängija on mängulaua lõpus.
	 */
	public void forward() 
			throws TooTiredToMoveException, IllegalMoveException {
		
		getBehavior().performForward(this);
	}
	
	/**
	 * Meetod mängija mängulaual liigutamiseks eelmisele
	 * mänguväljale.
	 * @throws TooTiredToMoveException Kui mängija vitaalsus on 0.
	 * @throws IllegalMoveException Kui mängija ei saa siseneda
	 * mänguväljale või kui mängija on mängulaua alguses.
	 */
	public void backward() 
			throws TooTiredToMoveException, IllegalMoveException 
			 {
		
		getBehavior().performBackward(this);
	}

	/**
	 * Meetod mängija vitaalsuse taastamiseks. Implementeeritud
	 * MoveBehavior klassis.
	 */
	public void sleep() {
		
		getBehavior().performSleep(this);
	}

	@Override
	public String toString() {
		return name;
	}

}
