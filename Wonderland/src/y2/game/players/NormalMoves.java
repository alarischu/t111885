package y2.game.players;

import y2.exceptions.IllegalMoveException;
import y2.exceptions.TooTiredToMoveException;
import y2.game.Tile;

/**
 * NormalMoves on MoveBehavior alamklass, milles tavaliikumist 
 * omav mängija kasutab vitaalsust ning mängija saab ilma 
 * magamata liikuda kuni 3 korda.
 * @author alari
 *
 */
public class NormalMoves extends MoveBehavior {
	
	/**
	 * Meetod tavaliikumise vitaalsuse vähenemise määra 
	 * saamiseks mänguväljade vahel liikumisel meetodiga 
	 * performMove().
	 * @return Määr, mille võrra väheneb mängija vitaalsus,
	 * kui ta liigub ühelt mänguväljalt teise.
	 */
	protected int getVitalityDecreaseAmount() {
		return 1;
	}
	
	/**
	 * Meetod tavaliikumise vaikimisi vitaalsuse saamiseks.
	 * @return Tavaliikumise vitaalsuse vaikeväärtus.
	 */
	protected int getInitialVitality() {
		return 3;
	}
	
	/**
	 * Meetod tavaliikumise minimaalse võimaliku
	 * vitaalsuse saamiseks.
	 * @return Min. võimalik mängija vitaalsus.
	 */
	protected int getMinVitality() {
		return 0;
	}

	/**
	 * Igal liikumisel väheneb mängija vitaalsus vastavalt
	 * getVitalityDecreaseAmount() määrale.
	 */
	protected void performMove(Player owner, Tile tile)
			throws IllegalMoveException, TooTiredToMoveException {
		
		if (tile == null) {
			throw new IllegalMoveException("Ei saa liikuda - " +
					"mänguväljak ei tohi olla null!");
		}
		
		if (tile == null || !tile.canEnter(owner)) {
			throw new IllegalMoveException("Ei saa siseneda sellele väljakule");
		}
		
		if (owner.getVitality() <= getMinVitality()) {
			throw new TooTiredToMoveException("Liiga väsinud, et liikuda..");
		}
		
		if (tile.equals(owner.getLocation())) {
			return;
		}
		
		owner.setVitality(owner.getVitality() - getVitalityDecreaseAmount());
		registerPlayer(owner, tile);
		
	}
	

	/**
	 * Taastatakse vitaalsuse vaikeväärtus 
	 * getInitialVitality().
	 */
	@Override
	protected void performSleep(Player owner) {
		
		owner.setVitality(getInitialVitality());
	}
}
