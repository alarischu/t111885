package y2.game.players;

import y2.exceptions.IllegalMoveException;
import y2.exceptions.TooTiredToMoveException;
import y2.game.Tile;

/**
 * UnlimitedMoves on MoveBehavior alamklass, milles 
 * mängija võib ilma väsimata liikuda lõputult.
 * @author alari
 *
 */
public class UnlimitedMoves extends MoveBehavior {

	@Override
	protected void performMove(Player owner, Tile tile)
			throws IllegalMoveException, TooTiredToMoveException {
		
		if (tile == null) {
			throw new IllegalMoveException("Ei saa liikuda - " +
					"mänguväljak ei tohi olla null!");
		}
		
		if (tile == null || !tile.canEnter(owner)) {
			throw new IllegalMoveException("Ei saa siseneda sellele väljakule");
		}
		
		if (tile.equals(owner.getLocation())) {
			return;
		}

		registerPlayer(owner, tile);
		
	}

	@Override
	protected void performSleep(Player owner) {
		// Do nothing.
		
	}

}
