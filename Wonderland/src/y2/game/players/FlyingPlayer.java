package y2.game.players;

import y2.game.Tile;

/**
 * FlyingPlayer on mängija (Player) alamliik, kes saab 
 * ilma puhkamata liikuda, kuid kelle kätte mahub 
 * korraga ainult 2 kaarti.
 * @author alari
 *
 */
public class FlyingPlayer extends Player {

	public FlyingPlayer(String name, Tile start) {
		super(name, start);
		setBehavior(new UnlimitedMoves());
	}
	
	@Override
	protected int getMaxCardsAllowed() {
		return 2;
	}

}
