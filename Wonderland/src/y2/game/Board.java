package y2.game;

/**
 * Imedemaa mänguväljak (Board) koosneb järjestikku 
 * asetatud mänguväljadest.
 * @author alari
 *
 */
public class Board {

	/**
	 * Viit järgmisele mänguväljale.
	 */
	private Tile start;
	/**
	 * Viit eelmisele mänguväljale.
	 */
	private Tile end;
	
	/**
	 * Meetod mängulaua algusväljaku saamiseks.
	 * @return Mängulaua alguses paiknev mänguväli.
	 */
	public Tile getStart() {
		return start;
	}
	
	/**
	 * Meetod mängulaua algusväljaku määramiseks.
	 * @param start	Mängulaua algusväljak.
	 */
	private void setStart(Tile start) {
		this.start = start;
	}
	
	/**
	 * Meetod mängulaua lõpuväljaku saamiseks.
	 * @return Mängulaua lõpus paiknev mänguväli.
	 * @return
	 */
	public Tile getEnd() {
		return end;
	}
	
	/**
	 * Meetod mängulaua lõpuväljaku määramiseks.
	 * @param end Mängulaua lõpuväljak.
	 */
	private void setEnd(Tile end) {
		this.end = end;
	}
	
	/**
	 * Meetod mängulaua ettevalmistamiseks ja 
	 * mänguväljakute mängulaua lõppu lisamiseks .
	 * @param tile Mängulaua lõppu lisatav mänguväli.
	 */
	public void append(Tile tile) {
		
		if (tile == null || contains(tile)) {
			return;
		}
		
		try {
			getEnd().setNext(tile);
			tile.setPrevious(getEnd());
			setEnd(tile);
			tile.setBoard(this);
		} catch (NullPointerException e) {
			setStart(tile);
			setEnd(tile);
		}
		
	}
	
	/**
	 * Meetod mängulaua väljakute mängijatest ja 
	 * mängukaartidest puhastamiseks.
	 */
	public void clear() {
		
		Tile tile = getStart();
		while (tile != getEnd()) {
			tile.clear();
			tile = tile.getNext();
		}
	}
	
	/**
	 * Meetod mängulaualt mänguväljaku otsimiseks.
	 * @param tile Otsitav mänguväljak.
	 * @return True, kui mängulaud sisaldab mänguvälja. 
	 * False, kui ei sisalda.
	 */
	public boolean contains(Tile tile) {
		
		Tile start = getStart();
		Tile end = getEnd();
		Tile iter = start;
		
		if (start == null) {
			return false;
		}
		
		while (!iter.equals(end)) {
			if (iter.equals(tile)) {
				return true;
			}
			iter = iter.getNext();
		}
		
		return false;
	}
	
	
}
