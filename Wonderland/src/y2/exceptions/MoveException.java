package y2.exceptions;

public class MoveException extends GameException {

	public MoveException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
